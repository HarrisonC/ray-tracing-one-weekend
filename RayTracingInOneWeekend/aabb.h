#pragma once

#include "ray.h"

class AABB
{
protected:
	Vector3f m_min;
	Vector3f m_max;

public:
	AABB() {}
	AABB(const Vector3f& min, const Vector3f& max) : m_min(min), m_max(max) {}
	AABB(Vector3f&& min, Vector3f&& max) : m_min(std::move(min)), m_max(std::move(max)) {}
	
	bool Hit(const Ray& ray, float& tMin, float& tMax) const
	{
		for (int i = 0; i < 3; ++i)
		{
			//Are they parallel? If so, handle it
			if (std::abs(ray.m_direction[i]) < FLT_EPSILON)
			{
				if (ray.m_origin[i] < m_min[i] || ray.m_origin[i] > m_max[i])
				{
					return false;
				}
			}
			//They're not parallel so see where ray intersects infinite plane
			else
			{
				const float invDir = 1.0f / ray.m_direction[i];
				float t0 = (m_min[i] - ray.m_origin[i]) * invDir;
				float t1 = (m_max[i] - ray.m_origin[i]) * invDir;
				if (invDir < 0.0f)
					std::swap(t0, t1);

				tMin = t0 > tMin ? t0 : tMin;
				tMax = t1 < tMax ? t1 : tMax;
				if (tMax <= tMin)
					return false;
			}
		}

		return true;
	}

	//----- Getters -----
	virtual const Vector3f& GetMin() const { return m_min; }
	virtual const Vector3f& GetMax() const { return m_max; }

	Vector3f FindMinBounds(const AABB& box2) const
	{
		return Vector3f{	std::min(m_min.x, box2.GetMin().x),
							std::min(m_min.y, box2.GetMin().y), 
							std::min(m_min.z, box2.GetMin().z) };
	}

	Vector3f FindMaxBounds(const AABB& box2) const
	{
		return Vector3f{	std::max(m_max.x, box2.GetMax().x),
							std::max(m_max.y, box2.GetMax().y),
							std::max(m_max.z, box2.GetMax().z) };
	}
};