#pragma once

#include <algorithm>
#include <vector>

#include "hitable.h"
#include "material.h"
#include "ray.h"

struct BoundingBox : public Hitable
{
	Vector3f m_min;
	Vector3f m_max;
	std::unique_ptr<Material> m_material;

	BoundingBox(const Vector3f& min, const Vector3f& max, std::unique_ptr<Material>&& material) :
		m_min(min), m_max(max), m_material(std::move(material)) {}

	virtual bool Hit(const Ray& ray, const float _tMin, const float _tMax, HitRecord& rec) const override
	{
		float tMin = _tMin;
		float tMax = _tMax;

		for (int i = 0; i < 3; ++i)
		{
			//Handle if they're parallel
			if (std::abs(ray.m_direction[i]) < FLT_EPSILON)
			{
				if (ray.m_origin[i] < m_min[i] || ray.m_origin[i] > m_max[i])
				{
					return false;
				}
			}
			//They're not parallel so they must collide at some point
			else
			{
				const float ood = 1.0f / ray.m_direction[i];
				float t1 = (m_min[i] - ray.m_origin[i]) * ood;
				float t2 = (m_max[i] - ray.m_origin[i]) * ood;

				if (t1 > t2)
				{
					std::swap(t1, t2);
				}

				//The pattern should always be min, min, min, max, max max
				//If it doesn't, then it can't hit
				tMin = std::max(t1, tMin);
				tMax = std::min(t2, tMax);

				if (tMin > tMax)
				{
					return false;
				}
			}
		}
		
		rec.t = tMin;
		rec.hitPoint = ray.PointAlongRay(rec.t);
		rec.material = m_material.get();
		rec.normal = FindNormal(rec.hitPoint);

		return true;
	}

	[[nodiscard]]
	Vector3f FindNormal(const Vector3f& hitPoint) const
	{
		const static std::vector<Vector3f> minNormals
		{
			Vector3f{ -1.0f,  0.0f,  0.0f },
			Vector3f{  0.0f, -1.0f,  0.0f },
			Vector3f{  0.0f,  0.0f, -1.0f }
		};

		const static std::vector<Vector3f> maxNormals
		{
			Vector3f{ 1.0f, 0.0f, 0.0f },
			Vector3f{ 0.0f, 1.0f, 0.0f },
			Vector3f{ 0.0f, 0.0f, 1.0f }
		};


		Vector3f delta = hitPoint - m_min;
		for (int i = 0; i < 3; i++)
		{
			const float dp = minNormals[i].Dot(delta);
			if (dp == 0 || std::abs(dp) <= 0.00001f)	//not pretty but avoiding rounding errors
			{
				return  minNormals[i];
			}
		}

		delta = hitPoint - m_max;
		for (int i = 0; i < 3; i++)
		{
			const float dp = maxNormals[i].Dot(delta);
			if (dp == 0 || std::abs(dp) <= 0.00001f)	//not pretty but avoiding rounding errors
			{
				return  maxNormals[i];
			}
		}

		return Vector3f{ 1.0f, 1.0f, 1.0f };
	}
};