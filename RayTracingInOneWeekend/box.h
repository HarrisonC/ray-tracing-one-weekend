#pragma once

#include "aabb.h"
#include "hitable.h"

#include <vector>

class Box : public Hitable
{
private:
	[[nodiscard]]
	Vector3f FindNormal(const Vector3f& hitPoint) const
	{
		const static std::vector<Vector3f> minNormals
		{
			Vector3f{ -1.0f,  0.0f,  0.0f },
			Vector3f{  0.0f, -1.0f,  0.0f },
			Vector3f{  0.0f,  0.0f, -1.0f }
		};

		const static std::vector<Vector3f> maxNormals
		{
			Vector3f{ 1.0f, 0.0f, 0.0f },
			Vector3f{ 0.0f, 1.0f, 0.0f },
			Vector3f{ 0.0f, 0.0f, 1.0f }
		};

		Vector3f delta = hitPoint - m_box.GetMin();
		for (int i = 0; i < 3; i++)
		{
			const float dp = minNormals[i].Dot(delta);
			if (Utils::CloseToOrIsZero(dp))	//not pretty but avoiding rounding errors
			{
				return minNormals[i];
			}
		}

		delta = hitPoint - m_box.GetMax();
		for (int i = 0; i < 3; i++)
		{
			const float dp = maxNormals[i].Dot(delta);
			if (Utils::CloseToOrIsZero(dp))	//not pretty but avoiding rounding errors
			{
				return maxNormals[i];
			}
		}

		return Vector3f{ 1.0f, 1.0f, 1.0f };
	}

private:
	AABB m_box;
	std::unique_ptr<Material>	m_material;

public:
	Box(const Vector3f& min, const Vector3f& max, std::unique_ptr<Material>	material)	: m_box(AABB(min, max)), m_material(std::move(material)) {}
	Box(Vector3f&& min, Vector3f&& max, std::unique_ptr<Material> material)				: m_box(AABB(min, max)), m_material(std::move(material)) {}

	bool Hit(const Ray& ray, const float tMin, const float tMax, HitRecord& rec) const override
	{
		float hitMinT = tMin;
		float hitMaxT = tMax;

		if (m_box.Hit(ray, hitMinT, hitMaxT))
		{
			rec.t = hitMinT;
			rec.hitPoint = ray.PointAlongRay(rec.t);
			rec.material = m_material.get();
			rec.normal = FindNormal(rec.hitPoint);
			rec.uv = Vector2f{ 0.0f, 0.0f }; //#TODO: FIX ME!

			return true;
		}

		return false;
	}

	bool BoundingBox(const float /*t0*/, const float /*t1*/, AABB& box) const override
	{
		box = m_box;
		return true;
	}
};