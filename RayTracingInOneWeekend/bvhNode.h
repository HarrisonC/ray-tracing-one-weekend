#pragma once

#include "hitable.h"
#include "aabb.h"
#include "utils.h"

#include <vector>

class BVH_Node : public Hitable
{
public:
	//#TODO: Not a big fan of these raw ptrs
	Hitable* m_leftChild = nullptr;
	Hitable* m_rightChild = nullptr;
	AABB	 m_box;

	 BVH_Node() {}
	 BVH_Node(std::vector<std::unique_ptr<Hitable>>& l, const unsigned int startIdx, const unsigned int length, const float time0, const float time1);

	virtual bool Hit(const Ray& ray, const float tMin, const float tMax, HitRecord& rec) const override;
	virtual bool BoundingBox(const float t0, const float t1, AABB& box) const override;
};

BVH_Node::BVH_Node(std::vector<std::unique_ptr<Hitable>>& l, const unsigned int startIdx, const unsigned int length, const float time0, const float time1)
{
	if (l.empty())
	{
		printf("We should never reach here... This will crash! \n");
		__debugbreak();
	}

	//Sort the hierarchy for splitting
	//Random from 0 to 2 (inclusive)
	const int axis = rand() % 2;
	auto sortFunc = [&axis](const std::unique_ptr<Hitable>& lhs, const std::unique_ptr<Hitable>& rhs) -> bool
	{
		AABB leftBox, rightBox;
		if (lhs->BoundingBox(0, 0, leftBox) == false || rhs->BoundingBox(0, 0, rightBox) ==  false)
		{
			printf("No bounding box for BVH Node ctor. This is very bad! \n");
			__debugbreak();
		}

		return leftBox.GetMin()[axis] - rightBox.GetMin()[axis] < 0.0f;
	};

	std::sort(l.begin() + startIdx, l.begin() + startIdx + length, sortFunc);

	//Now it's sorted, decide children
	if (length == 1)
	{
		m_leftChild = m_rightChild = l[startIdx].get();
	}
	else if(length == 2)
	{
		m_leftChild = l[startIdx].get();
		m_rightChild = l[startIdx + 1].get();
	}
	else
	{
		//We split the currently sorted list into 2 "halves"
		//One half is from start to the halfLength (approx.) and the second is the remainder from the previous split
		const unsigned int halfLength = length / 2;
		//#TODO: This memory leaks is awful. It is causing all sorts of ownership headaches
		//		 Do we own these children but not what we point when reaching the end of the tree? That's weird!
		m_leftChild = new BVH_Node(l, startIdx, halfLength, time0, time1);
		m_rightChild = new BVH_Node(l, startIdx + halfLength, length - halfLength, time0, time1);
	}

	AABB leftBox, rightBox;
	if (m_leftChild->BoundingBox(time0, time1, leftBox) == false ||
		m_rightChild->BoundingBox(time0, time1, rightBox) == false)
	{
		printf("No bounding box for BVH Node ctor. This is very bad! \n");
		__debugbreak();
	}

	m_box = Utils::CalculateSurroundingBox(leftBox, rightBox);
}

bool BVH_Node::Hit(const Ray& ray, const float tMin, const float tMax, HitRecord& rec) const
{
	float hitMinT = tMin;
	float hitMaxT = tMax;

	if (m_box.Hit(ray, hitMinT, hitMaxT))
	{
		HitRecord leftHitRecord, rightHitRecord;
		bool isLeftChildHit = m_leftChild->Hit(ray, tMin, tMax, leftHitRecord);
		bool isRightChildHit = m_rightChild->Hit(ray, tMin, tMax, rightHitRecord);
		if (isLeftChildHit && isRightChildHit)
		{
			rec = leftHitRecord.t < rightHitRecord.t ? leftHitRecord : rightHitRecord;
			return true;
		}
		else if (isLeftChildHit)
		{
			rec = leftHitRecord;
			return true;
		}
		else if (isRightChildHit)
		{
			rec = rightHitRecord;
			return true;
		}
	}

	return false;
}

bool BVH_Node::BoundingBox(const float /*t0*/, const float /*t1*/, AABB& box) const
{
	box = m_box;
	return true;
}
