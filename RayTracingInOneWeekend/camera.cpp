#include "camera.h"
#include "imgui_helper_functions.h"

#include <vector>

Camera::Camera(const Vector3f& position, const Vector3f& lookAt, const Vector3f& vUp, const float vfov, const float aspect, const float aperture, const float focusDist)	//vfov is top to bottom in degrees
{
	Initialise(position, lookAt, vUp, vfov, aspect, aperture, focusDist);
}

void Camera::Initialise(const Vector3f& position, const Vector3f& lookAt, const Vector3f& vUp, const float vfov, const float aspect, const float aperture, const float /*focusDist*/)
{
	m_lensRadius = aperture / 2;
	m_fov = vfov;
	m_position = position;
	m_lookAt = lookAt;
	m_up = vUp;

	Refresh(aspect);
}

void Camera::Refresh(const float aspect)
{
	float theta = static_cast<float>(m_fov * M_PI / 180.0f);
	m_halfHeight = tan(theta / 2);
	m_halfWidth = aspect * m_halfHeight;

	m_forward = Vector3f{ m_position - m_lookAt }.Normalised();
	m_right	  = m_up.Cross(m_forward).Normalised();
	m_up	  = m_forward.Cross(m_right);

	m_lowerLeftCorner = Vector3f{ m_position - (m_halfWidth * m_right) - (m_halfHeight * m_up) - m_forward };
	m_horizontal = Vector3f{ 2.0f * m_halfWidth * m_right };
	m_vertical = Vector3f{ 2.0f * m_halfHeight * m_up };

	//m_lowerLeftCorner = Vector3f{ m_position - m_halfWidth /**focusDist*/ * m_right - m_halfHeight /** focusDist*/ * m_up - /*focusDist **/ m_forward };	//This is hideous and not debuggable. Fix this!!!
	//m_horizontal	  = Vector3f{ 2 * m_halfWidth /** focusDist*/ * m_right };
	//m_vertical		  = Vector3f{ 2 * m_halfHeight /** focusDist*/ * m_up };
}

Ray Camera::GenRay(const float s, const float t) const
{
	//Vector3f rd = m_lensRadius * Utils::RandomPointInUnitDisk();
	//Vector3f offset = m_right * rd.x + m_up * rd.y;

	//Vector3f offset = { 0.0f };
	//return Ray{ m_position + offset, m_lowerLeftCorner + (s * m_horizontal) + (t * m_vertical)/* - m_position - offset*/ };

	return Ray{ m_position, m_lowerLeftCorner + (s * m_horizontal) + (t * m_vertical) - m_position };

}

void Camera::AddToImGui()
{
	if (ImGui::CollapsingHeader("Camera"))
	{
		if (ImGui::TreeNode("Positions"))
		{
			ImGuiHelper::CreateInputFloatBoxWithArrows(m_position, 0.25f);
			ImGui::TreePop();
		}

		if (ImGui::TreeNode("Look At"))
		{
			ImGuiHelper::CreateInputFloatBoxWithArrows(m_lookAt, 0.25f);
			ImGui::TreePop();
		}
	}
}