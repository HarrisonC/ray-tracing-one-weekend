#pragma once

#include "vector3.h"
#include "ray.h"
#include "imguiObject.h"

#define _USE_MATH_DEFINES
#include <math.h>

class Camera : public ImGuiObject
{
public:
	Camera(const Vector3f& position, const Vector3f& lookAt, const Vector3f& vUp, const float vfov, const float aspect, const float aperture, const float /*focusDist*/);	//vfov is top to bottom in degrees

	void	Initialise	(const Vector3f& position, const Vector3f& lookAt, const Vector3f& vUp, const float vfov, const float aspect, const float aperture, const float /*focusDist*/);
	void	Refresh		(const float aspect);
	Ray		GenRay		(const float s, const float t) const;

	//Getters
	const Vector3f& GetPosition	() const { return m_position; }
	const Vector3f& GetLookAt	() const { return m_lookAt; }

	void AddToImGui() override;

private:
	Vector3f m_lowerLeftCorner;
	Vector3f m_horizontal;
	Vector3f m_vertical;

	float	 m_halfWidth;
	float	 m_halfHeight;
	float	 m_fov;

	Vector3f m_position;
	Vector3f m_lookAt;

	Vector3f m_forward;
	Vector3f m_right;
	Vector3f m_up;

	float m_lensRadius;
};