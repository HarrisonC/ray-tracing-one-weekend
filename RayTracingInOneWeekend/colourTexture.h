#pragma once

#include "texture.h"
#include <memory>

class ColourTexture : public Texture
{
private:
	Vector3f m_colour;

public:
	 ColourTexture() {}
	 ColourTexture(const Vector3f& colour) : m_colour(colour) {}

	 Vector3f Value(const float /*u*/, const float /*v*/, const Vector3f& /*p*/) const override { return m_colour; }
};

class CheckerTexture : public Texture
{
private:
	std::unique_ptr<Texture> m_even;
	std::unique_ptr<Texture> m_odd;

public:
	CheckerTexture(const Vector3f& evenColour, const Vector3f& oddColour) :
		m_even(std::make_unique<ColourTexture>(evenColour)),
		m_odd(std::make_unique<ColourTexture>(oddColour))
	{}

	Vector3f Value(const float u, const float v, const Vector3f& p) const override
	{
		const float sines = sin(10 * p.x) * sin(10 * p.y) * sin(10 * p.z);
		if (sines < 0)
		{
			return m_odd->Value(u, v, p);
		}
		else
		{
			return m_even->Value(u, v, p);
		}
	}

};