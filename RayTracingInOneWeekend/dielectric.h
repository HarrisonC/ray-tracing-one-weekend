#pragma once

#include "material.h"
#include "utils.h"
#include "hitable.h"
#include "ray.h"

struct Dielectric : public Material
{
	Dielectric(const float refIdx) : m_refIdx(refIdx) {}
	virtual bool Scatter(const Ray& rayIn, HitRecord& record, Vector3f& attenuation, Ray& scattered) const;

	float m_refIdx;
};

bool Dielectric::Scatter(const Ray& rayIn, HitRecord& record, Vector3f& attenuation, Ray& scattered) const
{
	Vector3f outwardNormal;
	float ni_over_nt;
	Vector3f refracted;
	float cosine;
	float reflectProb;

	Vector3f reflected = Utils::Reflect(rayIn.m_direction, record.normal);
	attenuation = Vector3f{ 1.0f, 1.0f, 1.0f };

	if (rayIn.m_direction.Dot(record.normal) > 0)
	{
		outwardNormal = -record.normal;
		ni_over_nt = m_refIdx;
		cosine = m_refIdx * rayIn.m_direction.Dot(record.normal) / rayIn.m_direction.Magnitude();
	}
	else
	{
		outwardNormal = record.normal;
		ni_over_nt = 1.0f / m_refIdx;
		const float dp = rayIn.m_direction.Dot(record.normal);
		cosine = -dp / rayIn.m_direction.Magnitude();
	}

	if (Utils::Refract(rayIn.m_direction, outwardNormal, ni_over_nt, refracted))
	{
		reflectProb = Utils::Schlick(cosine, m_refIdx);
	}
	else
	{
		scattered = Ray{ record.hitPoint, reflected };
		reflectProb = 1.0f;
	}

	float randomN = (rand() % 100) / 100.0f;
	if (randomN < reflectProb)
	{
		scattered = Ray{ record.hitPoint, reflected };
	}
	else
	{
		scattered = Ray{ record.hitPoint, refracted };
	}

	return true;
}