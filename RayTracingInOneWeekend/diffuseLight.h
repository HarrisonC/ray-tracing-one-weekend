#pragma once

#include "material.h"
#include "texture.h"
#include <memory>

class DiffuseLight : public Material
{
private:
	std::unique_ptr<Texture> m_texture;

public:
	DiffuseLight(std::unique_ptr<Texture> texture) : m_texture(std::move(texture)) {}

	virtual bool Scatter(const Ray& /*rayIn*/, HitRecord& /*record*/, Vector3f& /*attenuation*/, Ray& /*scattered*/) const override
	{
		return false;
	}

	virtual Vector3f Emitted(const float u, const float v, const Vector3f& p) const override
	{
		return m_texture->Value(u, v, p);
	}
};
