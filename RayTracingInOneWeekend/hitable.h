#pragma once

#include "vector3.h"
#include "vector2.h"

struct Ray;
class  Material;
class AABB;

struct HitRecord
{
	float		t = 0.0f;
	Vector3f	hitPoint;
	Vector3f	normal;
	Vector2f	uv;
	Material*	material = nullptr;
};

struct Hitable
{
public:
	virtual ~Hitable() {}
	virtual bool Hit(const Ray& ray, const float tMin, const float tMax, HitRecord& rec) const = 0;
	virtual bool BoundingBox(const float t0, const float t1, AABB& box) const = 0;
};