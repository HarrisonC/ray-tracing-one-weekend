#pragma once

#include <vector>

#include "hitable.h"
#include "aabb.h"
#include "utils.h"

struct HitableList : public Hitable
{
	HitableList() {}
	HitableList(const int size) { m_hitableList.reserve(size); }
	
	std::vector<std::unique_ptr<Hitable>> m_hitableList;

	bool Hit(const Ray& ray, const float tMin, const float tMax, HitRecord& rec) const override
	{
		HitRecord tmpRecord;
		bool hitAnything = false;
		float closestSoFar = tMax;

		for (const auto& hitable : m_hitableList)
		{
			if (hitable->Hit(ray, tMin, closestSoFar, tmpRecord))
			{
				hitAnything = true;
				closestSoFar = tmpRecord.t;
				rec = tmpRecord;
			}
		}

		return hitAnything;
	};

	bool BoundingBox(const float t0, const float t1, AABB& box) const override
	{
		if (m_hitableList.empty()) 
			return false;

		AABB tmpBox;
		if (m_hitableList.front()->BoundingBox(t0, t1, tmpBox))
		{
			box = tmpBox;
		}
		else
		{
			return false;
		}

		for (int i = 0; i < m_hitableList.size(); ++i)
		{
			if (m_hitableList[i]->BoundingBox(t0, t1, tmpBox))
			{
				box = Utils::CalculateSurroundingBox(box, tmpBox);
			}
			else
			{
				return false;
			}
		}

		return true;
	}

	void AddObject	(std::unique_ptr<Hitable>&& object)
	{
		m_hitableList.push_back(std::move(object));
	}
};