#pragma once

#include "texture.h"
#include <SFML/Graphics/Image.hpp>

class ImageTexture : public Texture
{
private:
	//#TODO: Should this be a copy?
	sf::Image m_texture;

public:
	ImageTexture(const std::string& filepath)
	{
		m_texture.loadFromFile(filepath);
	}

	virtual Vector3f Value(const float u, const float v, const Vector3f& /*p*/) const override
	{
		const int width	 = static_cast<int>(m_texture.getSize().x);
		const int height = static_cast<int>(m_texture.getSize().y);

		const int i = std::clamp(int(u * width), 0, width - 1);
		const int j = std::clamp(int((1 - v) * height - 0.001f), 0, height - 1);

		sf::Color c = m_texture.getPixel(i, j);
		return Vector3f{ c.r / 255.0f, c.g / 255.0f, c.b / 255.0f };
	}
};