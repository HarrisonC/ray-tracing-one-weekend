#pragma once

struct ImGuiObject
{
	virtual void AddToImGui() = 0;
};