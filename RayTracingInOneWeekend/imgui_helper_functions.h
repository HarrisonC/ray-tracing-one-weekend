#pragma once

#include "ImGui/imgui.h"
#include "vector3.h"

namespace ImGuiHelper
{
	static inline void CreateInputFloatBoxWithArrows(float& n, const std::string& label, const float step)
	{
		const std::string hiddenLabel{ "##" + label };
		ImGui::Text(label.c_str());
		ImGui::SameLine();
		if (ImGui::ArrowButton(std::string{ hiddenLabel + "##left" }.c_str(), ImGuiDir_Left)) { n -= step; };
		ImGui::SameLine();
		ImGui::InputFloat(hiddenLabel.c_str(), &n);
		ImGui::SameLine();
		if (ImGui::ArrowButton(std::string{ label + "##right" }.c_str(), ImGuiDir_Right)) { n += step; };
	}

	static inline void CreateInputFloatBoxWithArrows(Vector3f& v, const float step)
	{
		CreateInputFloatBoxWithArrows(v.x, "X", step);
		CreateInputFloatBoxWithArrows(v.y, "Y", step);
		CreateInputFloatBoxWithArrows(v.z, "Z", step);
	}
}