#pragma once

#include <SFML/Graphics/Image.hpp>

#include "material.h"
#include "ray.h"
#include "hitable.h"
#include "utils.h"
#include "texture.h"
#include "colourTexture.h"

struct Lambertian : public Material
{
	Lambertian(std::unique_ptr<Texture>&& texture) : m_texture(std::move(texture))
	{
	}

	virtual bool Scatter(const Ray& hit, HitRecord& record, Vector3f& attenuation, Ray& scattered) const override;

	//#TODO: Do we really want this to be a unique_ptr?
	//I think yes as every texture can be different so any form of manager controlling lifetime would be grim
	//That being said, I'll leave this here so I can come back to it another time
	std::unique_ptr<Texture> m_texture;
};

bool Lambertian::Scatter(const Ray& /*rayIn*/, HitRecord& record, Vector3f& attenuation, Ray& scattered) const
{
	Vector3f target = record.hitPoint + record.normal + Utils::RandomPointInUnitSphere();
	scattered = Ray(record.hitPoint, target - record.hitPoint);
	attenuation = m_texture->Value(record.uv.x, record.uv.y, record.hitPoint);

	return true;
}
