#include <utility>
#include <functional>
#include <iostream>
#include <fstream>
#include <limits>
#include <random>
#include <time.h>

#include "vector2.h"
#include "ray.h"
#include "hitableList.h"
#include "sphere.h"
#include "camera.h"
#include "material.h"
#include "lambertian.h"
#include "metal.h"
#include "dielectric.h"
#include "renderLibrary.h"
#include "rayTracer.h"
#include "bvhNode.h"
#include "colourTexture.h"
#include "noiseTexture.h"
#include "imageTexture.h"
#include "diffuseLight.h"
#include "rect2D.h"
#include "box.h"
#include "translateHitable.h"
#include "rotateHitable.h"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>

#include "renderWindow.h"


template <typename T>
bool RunTests(const std::vector<std::pair<std::function<T()>, T>>& tests)
{
	for (const auto& test : tests)
	{
		T result = test.first();
		
		if (result != test.second)
			return false;
	}

	return true;
}

bool CrossTest()
{
	//-----------------------------------------------------------------------------------
	Vector3f af{ 1.0f, 0.0f, 0.0f }; Vector3f cf{ 2.0f, 4.0f, 2.0f };
	Vector3f bf{ 0.0f, 1.0f, 0.0f }; Vector3f df{ 4.0f, 2.0f, 2.0f };

	std::vector<std::pair<std::function<Vector3f()>, Vector3f>> floatTests;
	floatTests.emplace_back(std::make_pair([&]() -> Vector3f { return af.Cross(bf); },			Vector3f{ 0.0f, 0.0f, 1.0f }));
	floatTests.emplace_back(std::make_pair([&]() -> Vector3f { return cf.Cross(df); },			Vector3f{ 4.0f, 4.0f, -12.0f }));

	const bool floatSuccess = RunTests(floatTests);

	//-----------------------------------------------------------------------------------
	Vector3i ai{ 1, 0, 0 }; Vector3i ci{ 2, 4, 2 };
	Vector3i bi{ 0, 1, 0 }; Vector3i di{ 4, 2, 2 };

	std::vector<std::pair<std::function<Vector3i()>, Vector3i>> intTests;
	intTests.emplace_back(std::make_pair([&]() { return ai.Cross(bi); }, Vector3i{ 0, 0, 1 }));
	intTests.emplace_back(std::make_pair([&]() { return ci.Cross(di); }, Vector3i{ 4, 4, -12}));

	const bool intSuccess = RunTests(intTests);

	return floatSuccess && intSuccess;
}

bool DotTest()
{
	//-----------------------------------------------------------------------------------
	Vector2f af{ 1.0f, 0.0f }; Vector2f cf{ 2.0f, 4.0f };
	Vector2f bf{ 0.0f, 1.0f }; Vector2f df{ 2.0f, 4.0f };

	Vector3f ef{ 1.0f, 0.0f, 0.0f }; Vector3f gf{ 2.0f, 4.0f, 0.0f };
	Vector3f ff{ 0.0f, 1.0f, 0.0f }; Vector3f hf{ 2.0f, 4.0f, 0.0f };

	std::vector<std::pair<std::function<float()>, float>> floatTests;
	floatTests.emplace_back(std::make_pair([&]() { return af.Dot(bf); }, 0.0f));
	floatTests.emplace_back(std::make_pair([&]() { return cf.Dot(df); }, 20.0f));
	floatTests.emplace_back(std::make_pair([&]() { return ef.Dot(ff); }, 0.0f));
	floatTests.emplace_back(std::make_pair([&]() { return gf.Dot(hf); }, 20.0f));

	floatTests.emplace_back(std::make_pair([&]() { return af.Dot(cf); }, 2.0f));
	floatTests.emplace_back(std::make_pair([&]() { return bf.Dot(cf); }, 4.0f));
	floatTests.emplace_back(std::make_pair([&]() { return ef.Dot(gf); }, 2.0f));
	floatTests.emplace_back(std::make_pair([&]() { return ff.Dot(gf); }, 4.0f));


	const bool floatSuccess = RunTests(floatTests);
	//-----------------------------------------------------------------------------------
	Vector2i ai{ 1, 0 }; Vector2i ci{ 2, 4 };
	Vector2i bi{ 0, 1 }; Vector2i di{ 2, 4 };

	Vector3i ei{ 1, 0, 0 }; Vector3i gi{ 2, 4, 0};
	Vector3i fi{ 0, 1, 0 }; Vector3i hi{ 2, 4, 0};

	std::vector<std::pair<std::function<int()>, int>> intTests;
	intTests.emplace_back(std::make_pair([&]() { return ai.Dot(bi); }, 0));
	intTests.emplace_back(std::make_pair([&]() { return ci.Dot(di); }, 20));
	intTests.emplace_back(std::make_pair([&]() { return ei.Dot(fi); }, 0));
	intTests.emplace_back(std::make_pair([&]() { return gi.Dot(hi); }, 20));

	intTests.emplace_back(std::make_pair([&]() { return ai.Dot(ci); }, 2));
	intTests.emplace_back(std::make_pair([&]() { return bi.Dot(ci); }, 4));
	intTests.emplace_back(std::make_pair([&]() { return ei.Dot(gi); }, 2));
	intTests.emplace_back(std::make_pair([&]() { return fi.Dot(gi); }, 4));

	const bool intSuccess = RunTests(intTests);
	//-----------------------------------------------------------------------------------

	return floatSuccess && intSuccess;
}

void GenerateWorld(std::vector<std::unique_ptr<Hitable>>& world)
{
	using namespace Utils;

	world.push_back(std::make_unique<Sphere>(Vector3f{ 0.0f, -1000.0f, 0.0f }, 1000.0f, std::make_unique<Lambertian>(std::make_unique<CheckerTexture>( Vector3f{ 0.0f, 0.3f, 0.3f }, Vector3f{ 0.9f, 0.9f, 0.9f }))));

	for (int a = -8; a < 8; ++a)
	{
		for (int b = -8; b < 8; ++b)
		{
			const float chooseMaterial = GenRandom0And1();
			Vector3f centre{ a + 0.9f * GenRandom0And1(), 0.2f, b + 0.9f * GenRandom0And1() };

			if (chooseMaterial < 0.8f)
			{
				world.push_back(std::make_unique<Sphere>(centre, 0.2f, std::make_unique<Lambertian>(std::make_unique<ColourTexture>(Vector3f{ GenRandom0And1() * GenRandom0And1(), GenRandom0And1() * GenRandom0And1(), GenRandom0And1() * GenRandom0And1() }))));
			}
			else if (chooseMaterial <= 0.9f)
			{
				world.push_back(std::make_unique<Sphere>(centre, 0.2f, std::make_unique<Metal>(Vector3f{ 0.5f * (1.0f + GenRandom0And1()), 0.5f * (1.0f + GenRandom0And1()), 0.5f * (1.0f + GenRandom0And1()) }, 0.5f * GenRandom0And1())));
			}
			else
			{
				world.push_back(std::make_unique<Sphere>(centre, 0.2f, std::make_unique<Dielectric>(1.5f)));
			}
		}
	}

	world.push_back(std::make_unique<Sphere>(Vector3f{ 0.0f, 1.0f, 0.0f }, 1.0f, std::make_unique<Dielectric>(1.5f)));
	world.push_back(std::make_unique<Sphere>(Vector3f{ -4.0f, 1.0f, 0.0f }, 1.0f, std::make_unique<Lambertian>(std::make_unique<ColourTexture>(Vector3f{ 0.4f, 0.2f, 0.1f }))));
	world.push_back(std::make_unique<Sphere>(Vector3f{ 4.0f, 1.0f, 0.0f }, 1.0f, std::make_unique<Metal>(Vector3f{ 0.1f, 0.1f, 0.3f }, 0.0f)));
}

using WorldList = std::vector<std::unique_ptr<Hitable>>;
void GenerateWorldPerlin(WorldList& world)
{
	//world.push_back(std::make_unique<Sphere>(Vector3f{ 0.0f, -1000.0f, 0.0f }, 1000.0f, std::make_unique<Lambertian>(std::make_unique<NoiseTexture>(10.0f))));
	world.push_back(std::make_unique<Sphere>(Vector3f{ 0.0f, 2.0f, 0.0f }, 2.0f, std::make_unique<Lambertian>(std::make_unique<ImageTexture>("Assets/Textures/earth.jpg"))));
	//world.push_back(std::make_unique<Sphere>(Vector3f{ 0.0f, 2.0f, 0.0f }, 2.0f, std::make_unique<Lambertian>(std::make_unique<NoiseTexture>(10.0f))));
}

void GenerateWorldLight(WorldList& world)
{
	world.push_back(std::make_unique<Sphere>(Vector3f{ 0.0f, -1000.0f, 0.0f }, 1000.0f, std::make_unique<Lambertian>(std::make_unique<ColourTexture>(Vector3f{ 0.4f, 0.2f, 0.1f }))));
	world.push_back(std::make_unique<Sphere>(Vector3f{ 0.0f, 2.0f, 0.0f }, 2.0f, std::make_unique<Lambertian>(std::make_unique<ImageTexture>("Assets/Textures/earth.jpg"))));
	world.push_back(std::make_unique<Sphere>(Vector3f{ 0.0f, 14.0f, 0.0f }, 2.0f, std::make_unique<DiffuseLight>(std::make_unique<ColourTexture>(Vector3f{ 12.0f, 12.0f, 12.0f }))));
	world.push_back(std::make_unique<Rect2D>(Vector2f{ 3.0f, 1.0f }, Vector2f{ 5.0f, 3.0f }, Vector3f{ 1.0f, 0.0f, 0.0f }, 5.0f, std::make_unique<DiffuseLight>(std::make_unique<ColourTexture>(Vector3f{ 4.0f, 4.0f, 4.0f }))));
}

void GenerateWorldCornellBox(WorldList& world, Camera& cam)
{
	Vector3f green	{ 0.12f, 0.45f, 0.15f };
	Vector3f red	{ 0.65f, 0.05f, 0.05f  };
	Vector3f white	{ 0.73f };
	Vector3f light	{ 15.0f };

	world.push_back(std::make_unique<Rect2D>(Vector2f{ 0.0f, 0.0f },		Vector2f{ 555.0f, 555.0f }, Vector3f{-1.0f, 0.0f, 0.0f }, 555.0f,	std::make_unique<Lambertian>(std::make_unique<ColourTexture>(green))));
	world.push_back(std::make_unique<Rect2D>(Vector2f{ 0.0f, 0.0f },		Vector2f{ 555.0f, 555.0f }, Vector3f{ 1.0f, 0.0f, 0.0f }, 0.0f,		std::make_unique<Lambertian>(std::make_unique<ColourTexture>(red))));
	world.push_back(std::make_unique<Rect2D>(Vector2f{ 213.0f, 227.0f },	Vector2f{ 343.0f, 332.0f }, Vector3f{ 0.0f, 1.0f, 0.0f }, 554.0f,	std::make_unique<DiffuseLight>(std::make_unique<ColourTexture>(light))));
	world.push_back(std::make_unique<Rect2D>(Vector2f{ 0.0f, 0.0f },		Vector2f{ 555.0f, 555.0f }, Vector3f{ 0.0f, 1.0f, 0.0f }, 0.0f,		std::make_unique<Lambertian>(std::make_unique<ColourTexture>(white))));
	world.push_back(std::make_unique<Rect2D>(Vector2f{ 0.0f, 0.0f },		Vector2f{ 555.0f, 555.0f }, Vector3f{ 0.0f,-1.0f, 0.0f }, 555.0f,	std::make_unique<Lambertian>(std::make_unique<ColourTexture>(white))));
	world.push_back(std::make_unique<Rect2D>(Vector2f{ 0.0f, 0.0f },		Vector2f{ 555.0f, 555.0f }, Vector3f{ 0.0f, 0.0f,-1.0f }, 555.0f,	std::make_unique<Lambertian>(std::make_unique<ColourTexture>(white))));

	//world.push_back(std::make_unique<Box>(Vector3f{ 130.0f, 0.0f,  65.0f }, Vector3f{ 295.0f, 165.0f, 230.0f }, std::make_unique<Lambertian>(std::make_unique<ColourTexture>(white))));
	//world.push_back(std::make_unique<Box>(Vector3f{ 265.0f, 0.0f, 295.0f }, Vector3f{ 430.0f, 330.0f, 460.0f }, std::make_unique<Lambertian>(std::make_unique<ColourTexture>(white))));

	/*world.push_back(std::make_unique<TranslateHitable>
	(
		std::make_unique<RotateHitable>
		(
			std::make_unique<Box>
			(
				Vector3f{ 0.0f, 0.0f, 0.0f }, Vector3f{ 165.0f, 165.0f, 165.0f }, std::make_unique<Lambertian>(std::make_unique<ColourTexture>(white))
			),
			-18.0f
		),
		Vector3f{ 130.0f, 0.0f, 65.0f }
	));
	
	world.push_back(std::make_unique<TranslateHitable>
		(
			std::make_unique<RotateHitable>
			(
				std::make_unique<Box>
				(
					Vector3f{ 0.0f, 0.0f, 0.0f }, Vector3f{ 165.0f, 330.0f, 165.0f }, std::make_unique<Lambertian>(std::make_unique<ColourTexture>(white))
					),
				15.0f
				),
			Vector3f{ 265.0f, 0.0f, 295.0f }
	));*/

	world.push_back
	(
		std::make_unique<TranslateHitable>
		(
			std::make_unique<RotateHitable>
			(
				std::make_unique<Box>
				(
					Vector3f{ 0.0f, 0.0f, 0.0f }, Vector3f{ 165.0f, 165.0f, 165.0f }, std::make_unique<Lambertian>(std::make_unique<ColourTexture>(white))
				),
				-18.0f
			),
			Vector3f{ 130.0f, 0.0f, 65.0f }
		)
	);

	world.push_back
	(
		std::make_unique<TranslateHitable>
		(
			std::make_unique<RotateHitable>
			(
				std::make_unique<Box>
				(
					Vector3f{ 0.0f, 0.0f, 0.0f }, Vector3f{ 165.0f, 330.0f, 165.0f }, std::make_unique<Lambertian>(std::make_unique<ColourTexture>(white))
				),
				15.0f
			),
			Vector3f{ 265.0f, 0.0f, 295.0f }
		)
	);

	//--- Cam stuff
	const Vector3f camPosition{ 278.0f, 278.0f, -800.0f };
	const Vector3f camLookAt{ 278.0f, 278.0f, 0.0f };

	const float focusDistance = 10.0f;
	const float aperture = 0.0f;
	const float vfov = 40.0f;

	cam.Initialise(camPosition, camLookAt, Vector3f{ 0.0f, 1.0f, 0.0f }, vfov, float(200) / float(200), aperture, focusDistance);

}

Camera InitCamera()
{
	const Vector3f camPosition{ 0.0f, 2.0f, 3.0f };
	const Vector3f camLookAt{ 0.0f, 0.0f, -3.0f };

	const float focusDistance = Vector3f{ camPosition - camLookAt }.Magnitude();
	const float aperture = 2.0f;

	return { camPosition, camLookAt, Vector3f{ 0.0f, 1.0f, 0.0f }, 90.0f, float(200) / float(100), aperture, focusDistance };
}

int main()
{
	srand(static_cast<unsigned int>(time(nullptr)));

	RenderWindow	window{ { 1280, 720 } };
	RenderLibrary	renderLibrary;
	RayTracer		rayTracer(200, 100, 10);
	Camera			camera = InitCamera();
	//HitableList		world;

	window.AddObjectToImgui(renderLibrary);
	window.AddObjectToImgui(rayTracer);
	window.AddObjectToImgui(camera);

	//GenerateWorld(world);

	WorldList world;
	//GenerateWorld(world);
	//GenerateWorldPerlin(world);
	//GenerateWorldLight(world);
	GenerateWorldCornellBox(world, camera);
	BVH_Node bvhWorld = BVH_Node(world, 0, (unsigned int)world.size(), 1.0f, 1.0f);

	sf::Clock timer;
	while (window.IsOpen())
	{
		//If returns true, a render has been requested
		if (window.Update() || rayTracer.IsRequestingRender())
		{
			//If a request was sent (i.e. from a button) then 
			rayTracer.ResetRenderRequest();

			//Create a new image and refresh the camera
			camera.Refresh(rayTracer.GetAspect());

			//Cause a new render with the ray tracer
			rayTracer.RenderImage(camera, bvhWorld);

			//Add the image to the library so we can view it in ImGui for comparisons
			renderLibrary.AddImageToLibrary(LibraryImage(rayTracer.GetRenderImage(), rayTracer.GetSampleRate(), camera.GetPosition(), camera.GetLookAt()));

			window.SetTexture(renderLibrary.GetLastImage());
		}

		window.Draw();
	}

	return 0;
}