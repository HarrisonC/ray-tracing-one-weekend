#pragma once

#include "vector3.h"

struct HitRecord;
struct Ray;

class Material
{
public:

	virtual ~Material() {}
	virtual bool Scatter(const Ray& rayIn, HitRecord& record, Vector3f& attenuation, Ray& scattered) const = 0;
	virtual Vector3f Emitted(const float /*u*/, const float /*v*/, const Vector3f& /*p*/) const
	{
		return Vector3f{ 0.0f, 0.0f, 0.0f };
	}
};