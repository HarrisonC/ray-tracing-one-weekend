#pragma once

#include "material.h"
#include "hitable.h"
#include "ray.h"

struct Metal : public Material
{
	Metal(const Vector3f& albedo, const float fuzz) : m_albedo(albedo) { if (fuzz <= 1) m_fuzz = fuzz; else m_fuzz = 1.0f; }
	virtual bool Scatter(const Ray& hit, HitRecord& record, Vector3f& attenuation, Ray& scattered) const override;

	Vector3f m_albedo;
	float	 m_fuzz;
};


bool Metal::Scatter(const Ray& rayIn, HitRecord& record, Vector3f& attenuation, Ray& scattered) const
{
	Vector3f reflected = Utils::Reflect(rayIn.m_direction.Normalised(), record.normal);
	scattered = Ray(record.hitPoint, reflected + (m_fuzz * Utils::RandomPointInUnitSphere()));
	attenuation = m_albedo;

	return scattered.m_direction.Dot(record.normal) > 0;
}