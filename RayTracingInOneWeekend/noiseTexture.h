#pragma once

#include "texture.h"
#include "perlin.h"

class NoiseTexture : public Texture
{
private:
	Perlin		m_noise;
	const float m_scale;

public:
	NoiseTexture(const float scale) : m_scale(scale) {}

	virtual Vector3f Value(const float /*u*/, const float /*v*/, const Vector3f& p) const
	{
		return Vector3f(1.0f, 1.0f, 1.0f) * m_noise.noise(m_scale * p);
	}
};