#pragma once

#include <array>
#include <numeric>
#include "vector3.h"
#include "utils.h"

namespace
{
	constexpr size_t ARRAY_SIZE = 256;

	std::array<Vector3f, ARRAY_SIZE> PerlinGenerate()
	{
		std::array<Vector3f, ARRAY_SIZE> array;
		for (Vector3f& v : array)
		{
			v = Vector3f{ -1 + (2*Utils::GenRandom0And1()), -1 + (2 * Utils::GenRandom0And1()), -1 + (2 * Utils::GenRandom0And1()) }.Normalised();
		}

		return array;
	}

	void Permute(std::array<int, ARRAY_SIZE>& p)
	{
		for (unsigned int i = ARRAY_SIZE - 1; i > 0; --i)
		{
			const int target = int(Utils::GenRandom0And1() * (i + 1));
			std::swap(p[i], p[target]);
		}
	}

	std::array<int, ARRAY_SIZE> PerlinGeneratePerm()
	{
		std::array<int, ARRAY_SIZE> array;
		std::iota(array.begin(), array.end(), 0);
		Permute(array);

		return array;
	}

	std::array<int, ARRAY_SIZE * 2> PerlinLookup(const std::array<int, ARRAY_SIZE>& lookup)
	{
		std::array<int, ARRAY_SIZE * 2> array;
		for (int i = 0; i < 256; ++i)
		{
			array[256 + i] = array[i] = lookup[i];
		}

		return array;
	};

	float Fade(const float t)
	{
		return t * t * t * (t * (t * 6 - 15) + 10);
	}

	double Grad(const int hash, const double x, const double y, const double z)
	{
		const int h = hash & 15;
		const double u = h < 8 ? x : y;
		const double v = h < 4 ? y : h == 12 || h == 14 ? x : z;
		
		return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
	}
}

//#TODO: I'm very skeptical of this implementation, seems weird af to me
struct Perlin
{
	Perlin() = default;
	~Perlin() {}

	double noise(const Vector3f& p) const
	{
		//Peter Shirley Version
		/*const float u = p.x - std::floor(p.x);	
		const float v = p.y - std::floor(p.y);	
		const float w = p.z - std::floor(p.z);	
		const int i = int(4 * p.x) & 255;
		const int j = int(4 * p.y) & 255;
		const int k = int(4 * p.z) & 255;
		return m_gradients[m_permTableX[i] ^ m_permTableY[j] ^ m_permTableZ[k]];*/

		//Ken Perlin version
		/*const int cubeX = (int)std::floor(p.x) & 255;	//Remember that integer input will always return 0 so input should always be floats
		const int cubeY = (int)std::floor(p.y) & 255;	//By flooring the number, we're finding the related unit cube for the pixel
		const int cubeZ = (int)std::floor(p.z) & 255;

		const float x = p.x - std::floor(p.x);				//Find the point of the pixel relative within the unit cube
		const float y = p.y - std::floor(p.y);
		const float z = p.z - std::floor(p.z);

		const float u = Fade(x);
		const float v = Fade(y);
		const float w = Fade(z);

		int A = m_p[cubeX] + cubeY; int AA = m_p[A] + cubeZ; int AB = m_p[A + 1] + cubeZ;
		int B = m_p[cubeX + 1] + cubeY; int BA = m_p[B] + cubeZ; int BB = m_p[B + 1] + cubeZ;

		using namespace Utils;
		return Lerp(w,	Lerp(v, Lerp(u, Grad(m_p[AA  ], x  , y  , z   ),  // AND ADD
										Grad(m_p[BA  ], x-1, y  , z   )), // BLENDED
								Lerp(u, Grad(m_p[AB  ], x  , y-1, z   ),  // RESULTS
										Grad(m_p[BB  ], x-1, y-1, z   ))),// FROM  8
						Lerp(v, Lerp(u, Grad(m_p[AA+1], x  , y  , z-1 ),  // CORNERS
										Grad(m_p[BA+1], x-1, y  , z-1 )), // OF CUBE
								Lerp(u, Grad(m_p[AB+1], x  , y-1, z-1 ),
										Grad(m_p[BB+1], x-1, y-1, z-1 )))); */

		//Online Tutorial Version
		//Find unit cube X, Y, Z values (for top, near left)
		/*const int gridX = static_cast<int>(std::floor(p.x)) & 255;	
		const int gridY = static_cast<int>(std::floor(p.y)) & 255;
		const int gridZ = static_cast<int>(std::floor(p.z)) & 255;

		//Find the position of the point in unit cube
		const float innerPointX = p.x - std::floor(p.x);
		const float innerPointY = p.y - std::floor(p.y);
		const float innerPointZ = p.z - std::floor(p.z);

		//Calculate the unit cube values
		const float gridFreq = 1.0f;
		const Vector3f nearTopLeft{ (float)gridX, (float)gridY, (float)gridZ };
		const Vector3f nearTopRight{ (float)(gridX + gridFreq), (float)gridY, (float)gridZ };
		const Vector3f nearBottomLeft{ (float)gridX, (float)(gridY - gridFreq), (float)gridZ };
		const Vector3f nearBottomRight{ (float)(gridX + gridFreq), (float)(gridY - gridFreq), (float)gridZ };
		const Vector3f farTopLeft{ (float)gridX, (float)gridY, (float)(gridZ + gridFreq) };
		const Vector3f farTopRight{ (float)(gridX + gridFreq), (float)gridY, (float)(gridZ + gridFreq) };
		const Vector3f farBottomLeft{ (float)gridX, (float)(gridY - gridFreq), (float)(gridZ + gridFreq) };
		const Vector3f farBottomRight{ (float)(gridX + gridFreq), (float)(gridY - gridFreq), (float)(gridZ + gridFreq) };

		//Find unspecified "value" for each corner
		const int nearTopLeftValue = m_permTable[m_permTable[gridX] + gridY];
		const int nearTopRightValue = m_permTable[m_permTable[gridX + 1] + m_permTable[gridY + 1] + gridZ];
		const int nearBottomLeftValue = m_permTable[m_permTable[gridX] + m_permTable[gridY] + gridZ];
		const int nearBottomRightValue = m_permTable[m_permTable[gridX + 1] + m_permTable[gridY] + gridZ];

		const int farTopLeftValue = m_permTable[m_permTable[gridX] + m_permTable[gridY + 1] + gridZ + 1];
		const int farTopRightValue = m_permTable[m_permTable[gridX + 1] + m_permTable[gridY + 1] + gridZ + 1];
		const int farBottomLeftValue = m_permTable[m_permTable[gridX] + m_permTable[gridY] + gridZ + 1];
		const int farBottomRightValue = m_permTable[m_permTable[gridX + 1] + m_permTable[gridY] + gridZ + 1];

		auto GetConstantVector = [](const int v) -> Vector3f
		{ 
			const int h = v & 12;
			if (h == 0)
				return Vector3f{ 1.0f, 1.0f, 0.0f };
			else if (h == 1)
				return Vector3f{ -1.0f, 1.0f, 0.0f };

			else if (h == 2)
				return Vector3f{ 1.0f, -1.0f, 0.0f };
			else if (h == 3)
				return Vector3f{ -1.0f, -1.0f, 0.0f };

			else if (h == 4)
				return Vector3f{ 1.0f, 0.0f, 1.0f };
			else if (h == 5)
				return Vector3f{ -1.0f, 0.0f, 1.0f };

			else if (h == 6)
				return Vector3f{ 1.0f, 0.0f, -1.0f };
			else if (h == 7)
				return Vector3f{ -1.0f, 0.0f, -1.0f };

			else if (h == 8)
				return Vector3f{ 0.0f, 1.0f, 1.0f };
			else if (h == 9)
				return Vector3f{ 0.0f, -1.0f, 1.0f };

			else if (h == 10)
				return Vector3f{ 0.0f, 1.0f, -1.0f };
			else
				return Vector3f{ 0.0f, -1.0f, -1.0f };
		};

		const float dotNearTopLeft = nearTopLeft.Dot(GetConstantVector(nearTopLeftValue));
		const float dotNearTopRight = nearTopRight.Dot(GetConstantVector(nearTopRightValue));
		const float dotNearBottomLeft = nearBottomLeft.Dot(GetConstantVector(nearBottomLeftValue));
		const float dotNearBottomRight = nearBottomRight.Dot(GetConstantVector(nearBottomRightValue));
		const float dotFarTopLeft = farTopLeft.Dot(GetConstantVector(farTopLeftValue));
		const float dotFarTopRight = farTopRight.Dot(GetConstantVector(farTopRightValue));
		const float dotFarBottomLeft = farBottomLeft.Dot(GetConstantVector(nearBottomLeftValue));
		const float dotFarBottomRight = farBottomRight.Dot(GetConstantVector(nearBottomRightValue));

		const float u = Fade(innerPointX);
		const float v = Fade(innerPointY);
		const float w = Fade(innerPointZ);

		using namespace Utils;
		return Lerp(w,	Lerp(v,	Lerp(u, dotNearBottomLeft, dotNearTopLeft), 
								Lerp(u, dotNearBottomRight, dotNearTopRight)),
						Lerp(v, Lerp(u, dotFarBottomLeft, dotFarTopLeft),
								Lerp(u, dotFarBottomRight, dotFarTopRight)));*/

		//Online version									  
		const int cubeStartX = static_cast<int>(std::floor(p.x)) & 255;
		const int cubeStartY = static_cast<int>(std::floor(p.y)) & 255;
		const int cubeStartZ = static_cast<int>(std::floor(p.z)) & 255;
		const int cubeEndX = cubeStartX + 1;
		const int cubeEndY = cubeStartY + 1;
		const int cubeEndZ = cubeStartZ + 1;

		const float innerPointX = p.x - std::floor(p.x);
		const float innerPointY = p.y - std::floor(p.y);
		const float innerPointZ = p.z - std::floor(p.z);
		const float innerPointX_inv = innerPointX - 1.0f;
		const float innerPointY_inv = innerPointY - 1.0f;
		const float innerPointZ_inv = innerPointZ - 1.0f;

		const int hashA = m_p[cubeStartX];
		const int hashB = m_p[cubeEndX];
		const int hash00 = m_p[hashA + cubeStartY];
		const int hash01 = m_p[hashA + cubeEndY];
		const int hash10 = m_p[hashB + cubeStartY];
		const int hash11 = m_p[hashB + cubeEndY];

		const std::vector<Vector3f> gradients = 
		{
			Vector3f( 1.f, 1.f, 0.f),
			Vector3f(-1.f, 1.f, 0.f),
			Vector3f( 1.f,-1.f, 0.f),
			Vector3f(-1.f,-1.f, 0.f),
			Vector3f( 1.f, 0.f, 1.f),
			Vector3f(-1.f, 0.f, 1.f),
			Vector3f( 1.f, 0.f,-1.f),
			Vector3f(-1.f, 0.f,-1.f),
			Vector3f( 0.f, 1.f, 1.f),
			Vector3f( 0.f,-1.f, 1.f),
			Vector3f( 0.f, 1.f,-1.f),
			Vector3f( 0.f,-1.f,-1.f),

			Vector3f( 1.f, 1.f, 0.f),
			Vector3f(-1.f, 1.f, 0.f),
			Vector3f( 0.f,-1.f, 1.f),
			Vector3f( 0.f,-1.f,-1.f)

		};

		const Vector3f g000 = gradients[m_p[hash00 + cubeStartZ] & 15];
		const Vector3f g100 = gradients[m_p[hash10 + cubeStartZ] & 15];
		const Vector3f g010 = gradients[m_p[hash01 + cubeStartZ] & 15];
		const Vector3f g110 = gradients[m_p[hash11 + cubeStartZ] & 15];
		const Vector3f g001 = gradients[m_p[hash00 + cubeEndZ] & 15];
		const Vector3f g101 = gradients[m_p[hash10 + cubeEndZ] & 15];
		const Vector3f g011 = gradients[m_p[hash01 + cubeEndZ] & 15];
		const Vector3f g111 = gradients[m_p[hash11 + cubeEndZ] & 15];

		const float v000 = g000.Dot(Vector3f{ innerPointX,		innerPointY,		innerPointZ });
		const float v100 = g100.Dot(Vector3f{ innerPointX_inv,	innerPointY,		innerPointZ});
		const float v010 = g010.Dot(Vector3f{ innerPointX,		innerPointY_inv,	innerPointZ });
		const float v110 = g110.Dot(Vector3f{ innerPointX_inv,	innerPointY_inv,	innerPointZ });
		const float v001 = g000.Dot(Vector3f{ innerPointX,		innerPointY,		innerPointZ_inv });
		const float v101 = g100.Dot(Vector3f{ innerPointX_inv,	innerPointY,		innerPointZ_inv });
		const float v011 = g010.Dot(Vector3f{ innerPointX,		innerPointY_inv,	innerPointZ_inv });
		const float v111 = g110.Dot(Vector3f{ innerPointX_inv,	innerPointY_inv,	innerPointZ_inv });

		const float u = Fade(innerPointX);
		const float v = Fade(innerPointY);
		const float w = Fade(innerPointZ);


		using namespace Utils;
		const float l0 = Lerp(u, v000, v100);
		const float l1 = Lerp(u, v010, v110);
		const float l2 = Lerp(v, l0, l1);

		const float l3 = Lerp(u, v001, v101);
		const float l4 = Lerp(u, v011, v111);
		const float l5 = Lerp(v, l3, l4);

		return Lerp(w, l2, l5);

	}

	//static std::array<Vector3f, ARRAY_SIZE>	m_gradients;
	static std::array<int, ARRAY_SIZE>		m_permTable;
	static std::array<int, ARRAY_SIZE * 2>	m_p;

	//static std::array<int, ARRAY_SIZE>		m_permTableX;
	//static std::array<int, ARRAY_SIZE>		m_permTableY;
	//static std::array<int, ARRAY_SIZE>		m_permTableZ;
};

//std::array<Vector3f, ARRAY_SIZE> Perlin::m_gradients = PerlinGenerate();
std::array<int, ARRAY_SIZE>	 Perlin::m_permTable = PerlinGeneratePerm();
std::array<int, ARRAY_SIZE * 2> Perlin::m_p = PerlinLookup(Perlin::m_permTable);


//std::array<int, ARRAY_SIZE>	 Perlin::m_permTableX = PerlinGeneratePerm();
//std::array<int, ARRAY_SIZE>	 Perlin::m_permTableY = PerlinGeneratePerm();
//std::array<int, ARRAY_SIZE>	 Perlin::m_permTableZ = PerlinGeneratePerm();