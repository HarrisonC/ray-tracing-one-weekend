#pragma once

#include "vector3.h"

struct Ray
{
	 Ray(const Vector3f& origin, const Vector3f& dir) : m_origin(origin), m_direction(dir) {}
	 Ray() {}
	~Ray() {}

	Vector3f PointAlongRay(const float t) const { return Vector3f(m_origin + (m_direction * t)); }

	Vector3f m_origin;
	Vector3f m_direction;
};