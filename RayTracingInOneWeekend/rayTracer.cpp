#include "rayTracer.h"

#include <random>
#include <time.h>
#include <thread>
#include <functional>
#include <chrono>

#include "imgui_helper_functions.h"
#include "camera.h"
#include "renderedImage.h"
#include "hitable.h"
#include "material.h"


RayTracer::RayTracer(const int width, const int height, const int sampleRate) :
	m_width(width),
	m_height(height),
	m_sampleRate(sampleRate),
	m_requestRender(false),
	m_renderImage()
{
	m_numThreads = std::thread::hardware_concurrency();
	if (m_numThreads == 0)
	{
		m_numThreads = 1;
	}

	m_renderImage.create(m_width, m_height);
}

RayTracer::~RayTracer()
{
}

void RayTracer::RenderImage(const Camera& camera, Hitable& world)
{
	PerformPreRenderValidations();

	std::chrono::high_resolution_clock clock;
	const auto startTime = clock.now();

	printf("\n*****************\nStarted Rendering! - Using %d core(s) \n", m_numThreads);

	//Could add a minimum to reduce thread creation overhead but it's unlikely this would be necessary
	//as this code is very intensive so the overhead will almost always be worth it!
	//const int minAmountPerThread = 25;
	std::vector<std::thread> threads;
	threads.reserve(m_numThreads);

	const int blockW = m_width / m_numThreads;
	for (int i = 0; i < m_numThreads - 1; ++i)
	{
		threads.push_back(std::thread(&RayTracer::RenderBlock, *this, blockW, m_height, blockW * i, std::ref(m_renderImage), std::ref(camera), std::ref(world)));
	}

	//Need to do one last for the last block of the image,
	//this is for when the threads doesn't divide nicely into the width
	threads.push_back(std::thread(&RayTracer::RenderBlock, *this, m_width - (blockW * (m_numThreads - 1)), m_height, blockW * (m_numThreads - 1), std::ref(m_renderImage), std::ref(camera), std::ref(world)));

	std::for_each(threads.begin(), threads.end(), std::mem_fn(&std::thread::join));

	m_renderImage.flipVertically();

	//Print out render info
	printf("Finished Rendering! \n*****************\n");
	const auto timeTaken = std::chrono::duration_cast<std::chrono::milliseconds>(clock.now() - startTime).count();
	printf("Render Time - %llu(ms) \n", timeTaken);
}

//-------------------------------------------------------------------------------------------------
void RayTracer::RenderBlock(const int blockW, const int blockH, const int blockOffset, sf::Image& image, const Camera& camera, Hitable& world)
{
	for (int j = blockH - 1; j >= 0; --j)
	{
		for (int i = blockOffset; i < (blockOffset + blockW); ++i)
		{
			Vector3f colour;

			for (int s = 0; s < m_sampleRate; ++s)
			{
				float randomU = (rand() % 100) / 100.0f;
				float randomV = (rand() % 100) / 100.0f;

				float u = (i + randomU) / float(m_width);
				float v = (j + randomV) / float(m_height);

				Ray ray = camera.GenRay(u, v);
				colour += Colour(ray, world, 0);
			}

			colour /= float(m_sampleRate);
			colour = Vector3f(std::sqrt(colour.x), std::sqrt(colour.y), std::sqrt(colour.z));
			sf::Color finalColour{ sf::Uint8(255.99 * colour.x), sf::Uint8(255.99 * colour.y), sf::Uint8(255.99 * colour.z) };

			image.setPixel(i, j, finalColour);
		}
	}

	printf("Block Completed! \n");
}

//-------------------------------------------------------------------------------------------------
Vector3f RayTracer::Colour(const Ray& ray, Hitable& world, const int depth) const
{
	//static Vector3f startColour{ 1.0f, 1.0f, 1.0f };
	//static Vector3f endColour{ 0.5f, 0.7f, 1.0f };

	HitRecord record;
	if (world.Hit(ray, 0.001f, std::numeric_limits<float>::max(), record))
	{
		Ray scattered;
		Vector3f attenuation;
		const Vector3f emitted = record.material->Emitted(record.uv.x, record.uv.y, record.hitPoint);
		if (depth < 50 && record.material->Scatter(ray, record, attenuation, scattered))
		{
			return emitted + attenuation * Colour(scattered, world, depth + 1);
		}
		else
		{
			return emitted;
		}
	}
	else
	{
		/*Vector3f unitDirection = ray.m_direction.Normalised();
		float t = 0.5f * (unitDirection.y + 1.0f);
		return (1.0f - t) * startColour + t * endColour;*/
		return Vector3f{ 0.0f, 0.0f, 0.0f };
	}
}

//-------------------------------------------------------------------------------------------------
void RayTracer::PerformPreRenderValidations()
{
	if (m_renderImage.getSize().x != static_cast<unsigned int>(m_width) || m_renderImage.getSize().y != static_cast<unsigned int>(m_height))
	{
		m_renderImage.create(m_width, m_height);
	}
}

//-------------------------------------------------------------------------------------------------
void RayTracer::ResetRenderRequest()
{
	m_requestRender = false;
}

//-------------------------------------------------------------------------------------------------
bool RayTracer::IsRequestingRender() const
{
	return m_requestRender;
}

//------------------------------------------------------------------------------------------------ -
int RayTracer::GetWidth() const
{
	return m_width;
}
//------------------------------------------------------------------------------------------------ -
int RayTracer::GetHeight() const
{
	return m_renderImage.getSize().y;
}

//-------------------------------------------------------------------------------------------------
int RayTracer::GetSampleRate() const
{
	return m_sampleRate;
}

//-------------------------------------------------------------------------------------------------
float RayTracer::GetAspect() const
{ 
	return static_cast<float>(m_width) / static_cast<float>(m_height);
}

//-------------------------------------------------------------------------------------------------
void RayTracer::AddToImGui()
{
	if (ImGui::CollapsingHeader("Ray Tracer"))
	{
		ImGui::InputInt("Width##RayTracer",			&m_width);
		ImGui::InputInt("Height##RayTracer",		&m_height);
		ImGui::InputInt("Sample Rate##RayTracer",	&m_sampleRate);

		static const int maxThreads = std::thread::hardware_concurrency();
		if (maxThreads != 0)
		{
			if (ImGui::InputInt("Threads##RayTracer", &m_numThreads))
			{
				m_numThreads = std::clamp(m_numThreads, 1, maxThreads);
			}
		}

		if (ImGui::Button("Render##RayTracer"))
		{
			m_requestRender = true;
		}
	}
}