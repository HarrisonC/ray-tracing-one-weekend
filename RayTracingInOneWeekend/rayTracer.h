#pragma once

#include "vector3.h"
#include <SFML/Graphics/Image.hpp>
#include "imguiObject.h"

class  Camera;
struct Hitable;
struct Ray;

class RayTracer : public ImGuiObject
{
public:
	 RayTracer() = default;
	 RayTracer(const int width, const int height, const int sampleRate);
	~RayTracer();

	void RenderImage(const Camera& camera, Hitable& world);

	void AddToImGui() override;

	void ResetRenderRequest();
	bool IsRequestingRender() const;

	[[nodiscard]] int	GetSampleRate	() const;
	[[nodiscard]] float	GetAspect		() const;
	[[nodiscard]] int	GetWidth		() const;
	[[nodiscard]] int	GetHeight		() const;

	[[nodiscard]] const sf::Image& GetRenderImage() const { return m_renderImage; }

private:
	void		RenderBlock	(const int blockW, const int blockH, const int blockOffset, sf::Image& image, const Camera& camera, Hitable& world);
	Vector3f	Colour		(const Ray& ray, Hitable& world, const int depth) const;

	void		PerformPreRenderValidations();

	int m_width;
	int m_height;
	int	m_sampleRate;
	int	m_numThreads;

	bool m_requestRender;

	sf::Image m_renderImage;
};