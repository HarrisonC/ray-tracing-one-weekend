#pragma once

#include "vector2.h"
#include "material.h"
#include "hitable.h"
#include "ray.h"
#include "aabb.h"
#include <memory>


class Rect2D : public Hitable
{
private:
	Vector2f m_min;
	Vector2f m_max;
	Vector3f m_normal;

	unsigned int m_index;
	float		 m_depth;

	std::unique_ptr<Material> m_material;

public:
	Rect2D(const Vector2f& min, const Vector2f& max, const Vector3f& normal, const float depth, std::unique_ptr<Material>&& material) :
		m_min(min), m_max(max), m_normal(normal), m_depth(depth), m_material(std::move(material))
	{
		for (int i = 0; i < 3; ++i)
		{
			if (std::abs(m_normal[i]) == 1.0f)
			{
				m_index = i;
				break;
			}
		}
	}

	virtual bool Hit(const Ray& ray, const float tMin, const float tMax, HitRecord& rec) const override
	{
		//Calculate vector from ray origin to z of rect
		//Then accomodate for direction 
		//(i.e. if ray z dir is 0.5 then we need to x2 the length of the prev vector)
		const float t = (m_depth - ray.m_origin[m_index]) / ray.m_direction[m_index];
		if (t < tMin || t > tMax)
			return false;

		//It hit the plane so check if it within the bounds of the rect
		const Vector2f intersectP =  m_index == 0 ? Vector2f{ ray.m_origin[1] + t * ray.m_direction[1], ray.m_origin[2] + t * ray.m_direction[2] } :
									 m_index == 1 ? Vector2f{ ray.m_origin[0] + t * ray.m_direction[0], ray.m_origin[2] + t * ray.m_direction[2] } :
													Vector2f{ ray.m_origin[0] + t * ray.m_direction[0], ray.m_origin[1] + t * ray.m_direction[1] };
		
		if (intersectP.x < m_min.x || intersectP.x > m_max.x ||
			intersectP.y < m_min.y || intersectP.y > m_max.y)
		{
			return false;
		}
		
		//It hit so store the info in the hit record
		const float width = m_max.x - m_min.x;
		const float height = m_max.y - m_min.y;
		rec.t = t;
		rec.uv.x = (intersectP.x - m_min.x) / width;
		rec.uv.y = (intersectP.y - m_min.y) / height;
		rec.material = m_material.get();
		rec.hitPoint = ray.PointAlongRay(t);
		rec.normal = m_normal;

		return true;
	}

	virtual bool BoundingBox(const float /*t0*/, const float /*t1*/, AABB& box) const override
	{
		Vector3f min = { FLT_MAX };
		Vector3f max = { FLT_MAX };

		min[m_index] = m_depth - 0.001f;
		max[m_index] = m_depth + 0.001f;

		int j = 0;
		for (unsigned int i = 0; i < 3; ++i)
		{
			if(i != m_index)
			{
				min[i] = m_min[j];
				max[i] = m_max[j++];
			}
		}

		box = AABB(std::move(min), std::move(max));
		return true;
	}
};