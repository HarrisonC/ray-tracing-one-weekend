#include "renderLibrary.h"

#include "ImGui/imgui.h"
#include "ImGui/imgui-SFML.h"
#include "utils.h"

RenderLibrary::RenderLibrary()
{
}

RenderLibrary::~RenderLibrary()
{
}

//-------------------------------------------------------------------------------------------------
void RenderLibrary::AddImageToLibrary(LibraryImage&& image)
{
	m_renderLibrary.push_back(std::move(image));
}

//-------------------------------------------------------------------------------------------------
void RenderLibrary::RemoveImagesFromLibrary()
{
	for (uint32_t i = 0; i < m_renderLibrary.size(); ++i)
	{
		if (m_renderLibrary[i].m_markedForDelete)
		{
			m_renderLibrary.erase(m_renderLibrary.begin() + i);
			--i;
		}
	}
}

//-------------------------------------------------------------------------------------------------
const sf::Texture& RenderLibrary::GetLastImage() const
{
	return m_renderLibrary.back().m_texture;
}

//-------------------------------------------------------------------------------------------------
void RenderLibrary::AddToImGui()
{
	bool haveRemovedAnyImages = false;

	if (ImGui::CollapsingHeader("Image Library"))
	{
		uint32_t i = 0;
		std::string indexString;
		for(auto itr = m_renderLibrary.rbegin(); itr != m_renderLibrary.rend(); ++itr, ++i)
		{
			LibraryImage& image = *itr;
			indexString = std::to_string(i);

			//Resize images to be half the size
			//const sf::Vector2f resized = sf::Vector2f(image.m_texture.getSize()) * 0.5f;
			const sf::Vector2f resized = sf::Vector2f(100.0f, 50.0f);
			ImGui::Image(image.m_texture, resized);
			ImGui::SameLine();
			
			//Sets up the popup for exporting an image
			std::string exportPopupId = "ExportAsImage##" + indexString;
			ImGui::SetNextWindowSize({ 250.0f, 80.0f });
			if (ImGui::BeginPopup(exportPopupId.c_str()))
			{
				//Creates an input field so you can export the image to a specific directory
				static char filepath[128] = "Insert filename with folder(s) here...";
				ImGui::InputText("##filepath", filepath, 128);
				ImGui::NewLine();
				if (ImGui::Button("Export Image"))
				{
					const sf::Image exportImage = image.m_texture.copyToImage();
					if (exportImage.saveToFile(filepath))
					{
						printf("Successfully exported to '%s' \n", filepath);
					}
					else
					{
						printf("FAILED TO EXPORT IMAGE to '%s' \n", filepath);
					}

					ImGui::CloseCurrentPopup();
				}

				ImGui::EndPopup();
			}

			//Setup the popup that displays the image info (width, height and sample rate)
			std::string infoPopupId = "ImageInfo##" + indexString;
			if (ImGui::BeginPopup(infoPopupId.c_str()))
			{
				std::string currentInfo = "Width : " + std::to_string(image.GetWidth());
				ImGui::Text(currentInfo.c_str());

				currentInfo = "Height : " + std::to_string(image.GetHeight());
				ImGui::Text(currentInfo.c_str());

				currentInfo = "Sample Rate : " + std::to_string(image.m_sampleRate);
				ImGui::Text(currentInfo.c_str());

				currentInfo = "Camera Pos : " + Utils::AsString(image.GetCamPos());
				ImGui::Text(currentInfo.c_str());

				currentInfo = "Camera Look At : " + Utils::AsString(image.GetCamLookAt());
				ImGui::Text(currentInfo.c_str());

				ImGui::EndPopup();
			}

			//Create all the buttons for all the options
			std::string id = "Export##" + indexString;
			if (ImGui::Button(id.c_str()))
			{
				ImGui::OpenPopup(exportPopupId.c_str());
			}
			ImGui::SameLine();

			id = "Delete##"+ indexString;
			if (ImGui::Button(id.c_str()))
			{
				haveRemovedAnyImages = true;
				image.m_markedForDelete = true;
			}
			ImGui::SameLine();

			id = "Info##" + indexString;
			if (ImGui::Button(id.c_str()))
			{
				ImGui::OpenPopup(infoPopupId.c_str());
			}
		}
	}

	//If anything has been marked for delete, then go through and delete that image
	//Mark for delete could be used to mass delete images at some point
	if (haveRemovedAnyImages)
	{
		RemoveImagesFromLibrary();
	}
}

