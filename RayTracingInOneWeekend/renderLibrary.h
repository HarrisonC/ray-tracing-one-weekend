#pragma once

#include "renderedImage.h"
#include "imguiObject.h"

struct LibraryImage : public RenderedImage
{
	bool m_markedForDelete;

	LibraryImage(const sf::Image& image, const int sampleRate, const Vector3f& camPos, const Vector3f& camLookAt) :
		RenderedImage(image, sampleRate, camPos, camLookAt),
		m_markedForDelete(false)
	{
	}

	LibraryImage(sf::Texture&& texture, const int sampleRate, const Vector3f& camPos, const Vector3f& camLookAt) :
		RenderedImage(std::move(texture), sampleRate, camPos, camLookAt),
		m_markedForDelete(false) 
	{
	}

};

class RenderLibrary : public ImGuiObject
{
private:
	std::vector<LibraryImage> m_renderLibrary;


public:
	 RenderLibrary();
	~RenderLibrary();

	[[nodiscard]] const sf::Texture& GetLastImage() const;

	void AddImageToLibrary			(LibraryImage&& image);
	void RemoveImagesFromLibrary	();

	void AddToImGui					();
};