#include "renderWindow.h"
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/Texture.hpp>

RenderWindow::RenderWindow(sf::Vector2u extents) :
	m_window(sf::VideoMode{ extents.x, extents.y }, "HazCaz Ray Tracer"),
	m_sprite(),
	m_clock(),
	m_lastPos(sf::Mouse::getPosition()),
	m_settingsWindow(m_window)
{
	m_window.setFramerateLimit(60);
	m_sprite.setPosition({ 0.0f, 0.0f });
}

RenderWindow::~RenderWindow()
{
}

//-------------------------------------------------------------------------------------------------
bool RenderWindow::Update()
{
	sf::Event event;
	while (m_window.pollEvent(event))
	{
		m_settingsWindow.ProcessEvent(event);

		if (event.type == sf::Event::Closed || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		{
			m_window.close();
		}


		//If Ctrl + W is pressed, we regenerate the world
		//if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) && sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		//{
		//	GenerateWorld(world);
		//}

		if (event.type == sf::Event::MouseWheelMoved)
		{
			const float scaleDelta = event.mouseWheel.delta > 0 ? 0.25f : -0.25f;
			m_sprite.scale(1.0f + scaleDelta, 1.0f + scaleDelta);
		}

		//Allow dragging of the texture
		const sf::Vector2i currentMousePos = sf::Mouse::getPosition(m_window);
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && m_sprite.getGlobalBounds().contains((float)currentMousePos.x, (float)currentMousePos.y))
		{
			const sf::Vector2i delta = currentMousePos - m_lastPos;
			m_sprite.move((float)delta.x, (float)delta.y);
		}
		m_lastPos = currentMousePos;

		//Render with the ray tracer
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) && sf::Keyboard::isKeyPressed(sf::Keyboard::R))
		{
			return true;
		}
	}

	m_settingsWindow.Update(m_clock.restart());
	return false;
}

//-------------------------------------------------------------------------------------------------
void RenderWindow::Draw()
{
	//Render sprite holding render texture and ImGui to SFML window
	m_window.clear();

	m_window.draw(m_sprite);
	m_settingsWindow.Draw();

	m_window.display();
}

//-------------------------------------------------------------------------------------------------
void RenderWindow::AddObjectToImgui(ImGuiObject& object)
{
	m_settingsWindow.AddObject(object);
}

//-------------------------------------------------------------------------------------------------
void RenderWindow::SetTexture(const sf::Texture& texture)
{
	m_sprite.setTextureRect({ 0, 0, static_cast<int>(texture.getSize().x), static_cast<int>(texture.getSize().y) });
	m_sprite.setTexture(texture);
}
