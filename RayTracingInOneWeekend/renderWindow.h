#pragma once

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Image.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Clock.hpp>

#include "settingsWindow.h"

class RenderWindow
{
private:
	sf::RenderWindow	m_window;
	sf::Sprite			m_sprite;
	sf::Clock			m_clock;
	sf::Vector2i		m_lastPos;

	SettingsWindow		m_settingsWindow;

public:
	 RenderWindow(sf::Vector2u extents);
	~RenderWindow();

	//----- Utility Functions -----
	bool Update	();
	void Draw	();

	void AddObjectToImgui(ImGuiObject& object);

	//----- Setters ------
	void SetTexture(const sf::Texture& texture);

	//----- Getters -----
	const sf::Vector2u	GetWindowExtents	() const { return m_window.getSize(); }
	bool				IsOpen				() const { return m_window.isOpen(); }

};