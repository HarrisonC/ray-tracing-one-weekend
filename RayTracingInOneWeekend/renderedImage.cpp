#include "renderedImage.h"

RenderedImage::RenderedImage(sf::Texture&& texture, const int sampleRate, const Vector3f& camPos, const Vector3f& camLookAt) :
	m_texture(std::move(texture)),
	m_sampleRate(sampleRate),
	m_cameraPos(camPos),
	m_cameraLookAt(camLookAt)
{
}

RenderedImage::RenderedImage(const sf::Image& image, const int sampleRate, const Vector3f& camPos, const Vector3f& camLookAt) :
	m_texture(),
	m_sampleRate(sampleRate),
	m_cameraPos(camPos),
	m_cameraLookAt(camLookAt)
{
	m_texture.loadFromImage(image);
}

RenderedImage::~RenderedImage()
{
}

//-------------------------------------------------------------------------------------------------
int RenderedImage::GetWidth() const
{
	return m_texture.getSize().x;
}

//-------------------------------------------------------------------------------------------------
int RenderedImage::GetHeight() const
{
	return m_texture.getSize().y;
}

//-------------------------------------------------------------------------------------------------
const Vector3f& RenderedImage::GetCamPos() const
{
	return m_cameraPos;
}

//-------------------------------------------------------------------------------------------------
const Vector3f& RenderedImage::GetCamLookAt() const
{
	return m_cameraLookAt;
}