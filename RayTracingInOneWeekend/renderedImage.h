#pragma once

#include <SFML/Graphics/Texture.hpp>

#include "vector3.h"

namespace sf
{
	class Image;
}

struct RenderedImage
{
	sf::Texture m_texture;
	int			m_sampleRate;

	Vector3f	m_cameraPos;
	Vector3f	m_cameraLookAt;

	 RenderedImage(sf::Texture&& texture, const int sampleRate, const Vector3f& camPos, const Vector3f& camLookAt);
	 RenderedImage(const sf::Image& image, const int sampleRate, const Vector3f& camPos, const Vector3f& camLookAt);
	~RenderedImage();

	[[nodiscard]] int				GetWidth	 () const;
	[[nodiscard]] int				GetHeight	 () const;
	[[nodiscard]] const Vector3f&	GetCamPos	 () const;
	[[nodiscard]] const Vector3f&	GetCamLookAt () const;
};