#include "rotateHitable.h"

#define _USE_MATH_DEFINES
#include <math.h>

RotateHitable::RotateHitable(std::unique_ptr<Hitable>&& hitable, const float angleInDeg) :
	m_angleInDeg(angleInDeg),
	m_hitable(std::move(hitable)),
	m_rotAxis(Vector3f{ 0.0f, 1.0f, 0.0f })
{
	const float angleRad = (M_PI / 180.0) * m_angleInDeg;
	m_sinTheta = std::sin(angleRad);
	m_cosTheta = std::cos(angleRad);

	CalculateBoundingBox();
}

//-------------------------------------------------------------------------------------------------
bool RotateHitable::CalculateBoundingBox()
{
	m_hitable->BoundingBox(0.0f, 1.0f, m_aabb);

	Vector3f currentMin(FLT_MAX);
	Vector3f currentMax(-FLT_MAX);

	const Vector3f boxMin = m_aabb.GetMin();
	const Vector3f boxMax = m_aabb.GetMax();

	//WTF is this doing!?
	for (int i = 0; i < 2; ++i)
	{
		for (int j = 0; j < 2; ++j)
		{
			for (int k = 0; k < 2; ++k)
			{
				const float x = i * boxMax.x + (1 - i) * boxMin.x;
				const float y = j * boxMax.y + (1 - j) * boxMin.y;
				const float z = k * boxMax.z + (1 - k) * boxMin.z;
				//Y axis rotation - FIX THIS!
				const float newX =  m_cosTheta * x + m_sinTheta * z;
				const float newZ = -m_sinTheta * x + m_cosTheta * z;

				Vector3f testing{ newX, y, newZ };
				for (int n = 0; n < 3; n++)
				{
					currentMin[n] = std::min(testing[n], currentMin[n]);
					currentMax[n] = std::max(testing[n], currentMax[n]);
				}
			}
		}
	}

	m_aabb = AABB{ currentMin, currentMax };
	return true;
}

//-------------------------------------------------------------------------------------------------
bool RotateHitable::Hit(const Ray& ray, const float tMin, const float tMax, HitRecord& rec) const
{
	Vector3f rotatedOrigin = ray.m_origin;
	Vector3f rotatedDirection = ray.m_direction;

	rotatedOrigin[0] = m_cosTheta * ray.m_origin[0] - m_sinTheta * ray.m_origin[2];
	rotatedOrigin[2] = m_sinTheta * ray.m_origin[0] + m_cosTheta * ray.m_origin[2];
	rotatedDirection[0] = m_cosTheta * ray.m_direction[0] - m_sinTheta * ray.m_direction[2];
	rotatedDirection[2] = m_sinTheta * ray.m_direction[0] + m_cosTheta * ray.m_direction[2];

	const Ray rotatedRay{ rotatedOrigin, rotatedDirection };
	if (m_hitable->Hit(rotatedRay, tMin, tMax, rec))
	{
		Vector3f rotatedP = rec.hitPoint;
		Vector3f rotatedNormal = rec.normal;

		rotatedP[0] = m_cosTheta * rec.hitPoint[0] + m_sinTheta * rec.hitPoint[2];
		rotatedP[2] = -m_sinTheta * rec.hitPoint[0] + m_cosTheta * rec.hitPoint[2];
		rotatedNormal[0] = m_cosTheta * rec.normal[0] + m_sinTheta * rec.normal[2];
		rotatedNormal[2] = -m_sinTheta * rec.normal[0] + m_cosTheta * rec.normal[2];

		rec.hitPoint = rotatedP;
		rec.normal = rotatedNormal;

		return true;
	}

	return false;
}

//-------------------------------------------------------------------------------------------------
bool RotateHitable::BoundingBox(const float /*t0*/, const float /*t1*/, AABB& box) const
{
	box = m_aabb;
	return true;
}