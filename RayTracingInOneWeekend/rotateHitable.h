#pragma once

#include "hitable.h"
#include "aabb.h"

class RotateHitable : public Hitable
{
private:
	bool CalculateBoundingBox();

private:
	//#TODO: Again, worried thatr unique ptr is not the right choice here
	//Do we own the shape or are we merely a wrapper? For now, we're going to own it
	float					 m_angleInDeg;
	float					 m_sinTheta;
	float					 m_cosTheta;
	std::unique_ptr<Hitable> m_hitable;
	Vector3f				 m_rotAxis;
	AABB					 m_aabb;

public:
	RotateHitable(std::unique_ptr<Hitable>&& hitable, const float angleInDeg);

	virtual bool Hit			(const Ray& ray, const float tMin, const float tMax, HitRecord& rec) const override;
	virtual bool BoundingBox	(const float t0, const float t1, AABB& box) const override;
};