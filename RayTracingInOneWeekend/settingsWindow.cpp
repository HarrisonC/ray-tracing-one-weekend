#include "settingsWindow.h"

#include "imguiObject.h"
#include "ImGui/imgui.h"
#include "ImGui/imgui-SFML.h"

#include <SFML/Graphics/RenderWindow.hpp>

SettingsWindow::SettingsWindow(sf::RenderWindow& window) :
	m_window(window)
{
	ImGui::SFML::Init(window);
}

SettingsWindow::~SettingsWindow()
{
	ImGui::SFML::Shutdown();
}

//-------------------------------------------------------------------------------------------------
void SettingsWindow::Update(sf::Time time)
{
	ImGui::SFML::Update(m_window, time);

	ImGui::Begin("Debug Tool");
	ImGui::SetNextWindowSize({ static_cast<float>(m_window.getSize().x), static_cast<float>(m_window.getSize().y) });
	std::for_each(m_objects.begin(), m_objects.end(), [](ImGuiObject& object) { object.AddToImGui(); });
	ImGui::End();
}

//-------------------------------------------------------------------------------------------------
void SettingsWindow::ProcessEvent(const sf::Event& event)
{
	ImGui::SFML::ProcessEvent(event);
}

//-------------------------------------------------------------------------------------------------
void SettingsWindow::Draw()
{
	ImGui::SFML::Render(m_window);
	ImGui::EndFrame();
}

//-------------------------------------------------------------------------------------------------
void SettingsWindow::AddObject(ImGuiObject& object)
{
	m_objects.push_back(object);
}
