#pragma once

#include <vector>

namespace sf
{
	class RenderWindow;
	class Time;
	class Event;
}

struct ImGuiObject;

class SettingsWindow
{
private:
	std::vector<std::reference_wrapper<ImGuiObject>> m_objects;
	sf::RenderWindow&				m_window;

public:
	 SettingsWindow(sf::RenderWindow& window);
	~SettingsWindow();

	void Initialise(sf::RenderWindow& window);

	void ProcessEvent(const sf::Event& event);
	void Update(sf::Time time);
	void Draw();

	void AddObject(ImGuiObject& object);
};