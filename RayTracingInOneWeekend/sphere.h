#pragma once

#define _USE_MATH_DEFINES
#include <math.h>

#include "hitable.h"
#include "ray.h"
#include "aabb.h"

class Material;

namespace
{
	Vector2f GetSphereUV(const Vector3f& p)
	{
		//Formula is 
		//u = phi / (2 * pi)
		//v = theta / pi

		//Find the latitude
		//The reason for tan is because we have the proportional (x = cos(phi)) and (y = sin(phi)) and tan finds the tangent of these
		//We then inverse it to find the relational angle but returns in range of -pi and +pi so needs converting later
		const float phi = atan2(p.z, p.x);
		//Next find the longitude
		//I believe this is sine because we're finding the length of the opposite when looking at vector PC (P - C)
		const float theta = asin(p.y);

		//I can only assume all the crazy non-formula stuff is converting to be in the range of 0-1
		return Vector2f{ 1 - (phi + (float)M_PI) / (2 * (float)M_PI), (theta + (float)M_PI / 2) / (float)M_PI };
	}
}

struct Sphere : public Hitable
{
	Sphere() : m_centre({}), m_radius(0.0f), m_material(nullptr) {}
	Sphere(const Vector3f& centre, const float radius) : m_centre(centre), m_radius(radius), m_material(nullptr) {}
	Sphere(const Vector3f& centre, const float radius, std::unique_ptr<Material>&& material) : m_centre(centre), m_radius(radius), m_material(std::move(material)) {}

	Vector3f					m_centre;
	float						m_radius;
	std::unique_ptr<Material>	m_material;

	virtual bool Hit(const Ray& ray, const float tMin, const float tMax, HitRecord& rec) const override
	{
		//Equation - with t as the unknown collision point along r
		//Originally was dot((p(t) - C), (p(t) - C) = R * R
		//which expands into: dot((A + t * B-C), (A + t * B-C)) = R * R
		//Solving for t gives:
		//t * t * dot(B, B) + 2 * t * dot(B, A-C) + dot(A-C, A-C) - R * R = 0
		//discriminant is part of quadratic formula which reveals if there are 0 (negative), 1 (0) or 2 (positive) solutions
		//b*b - 4ac

		const Vector3f oc = ray.m_origin - m_centre;
		const float a = ray.m_direction.Dot(ray.m_direction);
		const float b = oc.Dot(ray.m_direction);
		const float c = oc.Dot(oc) - (m_radius * m_radius);

		const float discriminant = b * b - a * c;
		if (discriminant > 0.0f)
		{
			float quadSum = (-b - std::sqrt(discriminant)) / a;
			if (quadSum < tMin || quadSum > tMax)
			{
				quadSum = (-b + std::sqrt(discriminant)) / a;
			}

			if (quadSum > tMin&& quadSum < tMax)
			{
				rec.t = quadSum;
				rec.hitPoint = ray.PointAlongRay(rec.t);
				rec.normal = (rec.hitPoint - m_centre) / m_radius;
				rec.material = m_material.get();
				
				Vector3f modelPos = Vector3f{ rec.hitPoint - m_centre }.Normalised();
				rec.uv = Vector2f{ (float)(std::atan2(modelPos.x, modelPos.z) / (2 * M_PI) + 0.5f), modelPos.y * 0.5f + 0.5f };

				return true;
			}
		}

		return false;
	}

	virtual bool BoundingBox(const float /*t0*/, const float /*t1*/, AABB& box) const override
	{
		box = AABB{ Vector3f{ m_centre - m_radius }, Vector3f{ m_centre + m_radius } };
		return true;
	}
};