#pragma once

#include "vector3.h"

class Texture
{
public:
	virtual ~Texture() {}

	virtual Vector3f Value(const float u, const float v, const Vector3f& p) const = 0;
};