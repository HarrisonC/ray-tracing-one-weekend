#pragma once

#include "hitable.h"
#include "ray.h"
#include "aabb.h"

class TranslateHitable : public Hitable
{
private:
	std::unique_ptr<Hitable> m_hitable;
	Vector3f				 m_offset;

public:
	TranslateHitable(std::unique_ptr<Hitable>&& hitable, const Vector3f& offset) :
		m_hitable(std::move(hitable)),
		m_offset(offset)
	{}

	virtual bool Hit(const Ray& ray, const float tMin, const float tMax, HitRecord& rec) const
	{
		const Ray offsetRay{ ray.m_origin - m_offset, ray.m_direction };
		if (m_hitable->Hit(offsetRay, tMin, tMax, rec))
		{
			rec.hitPoint += m_offset;
			return true;
		}

		return false;
	}

	virtual bool BoundingBox(const float t0, const float t1, AABB& box) const
	{
		if (m_hitable->BoundingBox(t0, t1, box))
		{
			box = AABB(box.GetMin() + m_offset, box.GetMax() + m_offset);
			return true;
		}

		return false;
	}

};