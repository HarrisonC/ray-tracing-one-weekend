#include "utils.h"
#include "aabb.h"

#define _USE_MATH_DEFINES
#include <math.h>

namespace Utils
{
	double	Lerp(const double t, const double a, const double b) { return a + t * (b - a); }
	bool	CloseToOrIsZero(const float n)						 { return n == 0 || std::abs(n) <= 0.0001f; }

	float GenRandom0And1()
	{
		return (rand() % 100) / 100.0f;
	}

	Vector3f GenRandomVector0And1()
	{
		return Vector3f{ GenRandom0And1(), GenRandom0And1(), GenRandom0And1() };
	}

	Vector3f RandomPointInUnitSphere()
	{
		Vector3f p;

		do
		{
			p = 2.0f * Vector3f{ (rand() % 100) / 100.0f, (rand() % 100) / 100.0f, (rand() % 100) / 100.0f } -Vector3f{ 1.0f };
		} while (p.MagnitudeSquared() >= 1.0f);

		return p;
	}

	Vector3f RandomPointInUnitDisk()
	{
		Vector3f p;

		do
		{
			p = 2.0f * Vector3f{ (rand() % 100) / 100.0f, (rand() % 100) / 100.0f, 0.0f } -Vector3f{ 1.0f, 1.0f, 0.0f };
		} while (p.Dot(p) >= 1.0f);

		return p;
	}

	Vector3f Reflect(const Vector3f& in, const Vector3f normal)
	{
		return in - 2.0f * in.Dot(normal) * normal;
	}

	bool Refract(const Vector3f& in, const Vector3f normal, const float ni_over_nt, Vector3f& refracted)
	{
		Vector3f uv = in.Normalised();
		float dt = uv.Dot(normal);
		float discriminant = 1.0f - ni_over_nt * ni_over_nt * (1 - dt * dt);
		if (discriminant > 0)
		{
			refracted = ni_over_nt * (uv - normal * dt) - normal * std::sqrt(discriminant);
			return true;
		}

		return false;
	}

	float Schlick(const float cosine, const float refIdx)
	{
		float r0 = (1 - refIdx) / (1 + refIdx);
		r0 = r0 * r0;
		return r0 + (1 - r0) * std::pow((1 - cosine), 5);
	}

	std::string AsString(const Vector3f& v)
	{
		return std::string{ std::to_string(v.x) + ", " + std::to_string(v.y) + ", " + std::to_string(v.z) };
	}

	AABB CalculateSurroundingBox(const AABB& box1, const AABB& box2)
	{
		return AABB{ box1.FindMinBounds(box2), box1.FindMaxBounds(box2) };
	}

	namespace Lighting
	{
		//Equation: surfaceRoughness ^ 2 / PI(( normal . halfVector)^2 * (surfaceRoughness^2 - 1) + 1)^2
		float DistributionGGX(const Vector3f& normal, const Vector3f& halfVector, const float surfaceRoughness)
		{
			const float roughness4 = std::pow(surfaceRoughness, 4);
			const float nDotH2 = std::pow(normal.Dot(halfVector), 2);

			const float nom = roughness4;
			float denom = nDotH2 * (roughness4 - 1.0f) + 1.0f;
			denom = static_cast<float>(M_PI) * denom * denom;

			return nom / denom;
		}

		//Assuming direct lighting and not IBL lighting - will probably add a bool or override in the future
		//Equation: nDotV / nDotV * (1 - k) + k
		float GeometrySchlickGGX(const float nDotV, const float surfaceRoughness)
		{
			const float remappedRoughness = std::pow(surfaceRoughness + 1.0f, 2) / 8.0f;

			const float num = nDotV;
			const float denom = nDotV * (1.0f - remappedRoughness) + remappedRoughness;

			return num / denom;
		}

		float GeometrySmith(const Vector3f& normal, const Vector3f& viewDirection, const Vector3f& lightDirection, const float roughness)
		{
			const float nDotV = std::max(normal.Dot(viewDirection), 0.0f);
			const float nDotL = std::max(normal.Dot(lightDirection), 0.0f);

			const float ggx1 = GeometrySchlickGGX(nDotV, roughness);
			const float ggx2 = GeometrySchlickGGX(nDotL, roughness);

			return ggx1 * ggx2;
		}

		//Equation: F0 + (1 - F0) * (1 - (h . v))^5
		Vector3f FresnelSchlick(const float cosTheta, const Vector3f& F0)
		{
			return F0 + (1.0f - F0) * std::pow(1.0f - cosTheta, 5);
		}
	}
}