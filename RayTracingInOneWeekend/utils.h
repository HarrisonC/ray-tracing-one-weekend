#pragma once

#include "vector3.h"

class AABB;

namespace Utils
{
	double Lerp(const double t, const double a, const double b);

	bool CloseToOrIsZero					(const float n);

	float			GenRandom0And1			();
	Vector3f		GenRandomVector0And1	();

	Vector3f		RandomPointInUnitSphere	();
	Vector3f		RandomPointInUnitDisk	();
	Vector3f		Reflect					(const Vector3f& in, const Vector3f normal);
	bool			Refract					(const Vector3f& in, const Vector3f normal, const float ni_over_nt, Vector3f& refracted);
	float			Schlick					(const float cosine, const float refIdx);
	std::string		AsString				(const Vector3f& v);
	AABB			CalculateSurroundingBox (const AABB& box1, const AABB& box2);

	namespace Lighting
	{
		float		DistributionGGX		(const Vector3f& normal, const Vector3f& halfVector, const float surfaceRoughness);
		float		GeometrySchlickGGX	(const float nDotV, const float surfaceRoughness);
		float		GeometrySmith		(const Vector3f& normal, const Vector3f& viewDirection, const Vector3f& lightDirection, const float roughness);
		Vector3f	FresnelSchlick		(const float cosTheta, const Vector3f& F0);
	}
}
