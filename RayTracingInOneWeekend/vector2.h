#pragma once

#include <cmath>
#include <assert.h>

template <typename T>
struct Vector2
{
	T x;
	T y;

	constexpr Vector2() 
	{
		x = 0; y = 0;
	}

	constexpr explicit Vector2(const T _x, const T _y) 
		:	x(_x), y(_y)
	{}

	//----- Utility Functions -----
	constexpr float MagnitudeSquared() const
	{
		return (x * x) + (y * y);
	}

	constexpr float Magnitude() const
	{
		return std::sqrt(MagnitudeSquared());
	}

	constexpr Vector2<T> Normalised()	const
	{
		return Vector2<T>{ this / Magnitude() };
	}

	constexpr T Dot(const Vector2<T>& rhs)	const
	{
		return (x * rhs.x) + (y * rhs.y);
	}

	constexpr float AbsDot(const Vector2<T>& rhs)	const
	{
		return std::abs(Dot(rhs));
	}

	constexpr Vector2<T> ProjectionOnto(const Vector2<T>& rhs) const
	{
		const float dot = Dot(rhs);
		return (dot / rhs.MagnitudeSquared()) * rhs;
	}

	//----- Accessor Functions -----
	constexpr T X() const
	{
		return x;
	}

	constexpr T Y() const
	{
		return y;
	}

	//----- Operator Overloads -----
	constexpr Vector2<T> operator-() const
	{
		return { -x, -y };
	}

	const T operator[](const int i) const
	{
		assert(i >= 0 && i < 2);

		if (i == 0) return x;
		return y;
	}

	constexpr bool operator== (const Vector2<T>& rhs) const
	{
		return x == rhs.x && y == rhs.y;
	}

	constexpr bool operator!= (const Vector2<T>& rhs) const
	{
		return x != rhs.x || y != rhs.y;
	}

	constexpr Vector2<T> operator+ (const Vector2<T>& rhs) const
	{
		return Vector2<T>{ x + rhs.x, y + rhs.y };
	}

	constexpr Vector2<T>& operator+= (const Vector2<T>& rhs)
	{
		x += rhs.x; y += rhs.y;
		return *this;
	}

	constexpr Vector2<T> operator- (const Vector2<T>& rhs) const
	{
		return Vector2<T>{ x - rhs.x, y - rhs.y };
	}

	constexpr Vector2<T>& operator-= (const Vector2<T>& rhs)
	{
		x -= rhs.x; y -= rhs.y;
		return *this;
	}

	constexpr Vector2<T> operator* (const Vector2<T>& rhs) const
	{
		return Vector2<T>{ x * rhs.x, y * rhs.y };
	}

	constexpr Vector2<T>& operator*= (const Vector2<T>& rhs)
	{
		x *= rhs.x; y *= rhs.y;
		return *this;
	}

	constexpr Vector2<T> operator* (const T& n) const
	{
		return { x * n, y * n };
	}

	constexpr Vector2<T>& operator*= (const T& n)
	{
		x *= n; y *= n;
		return *this;
	}

	constexpr Vector2<T> operator/ (const Vector2<T>& rhs) const
	{
		assert(rhs.x != 0 && rhs.y != 0);
		return Vector2<T>{ x / rhs.x, y / rhs.y };
	}

	constexpr Vector2<T>& operator/= (const Vector2<T>& rhs)
	{
		assert(rhs.x != 0 && rhs.y != 0);

		x /= rhs.x; y /= rhs.y;
		return *this;
	}

	constexpr Vector2<T> operator/ (const T& n) const
	{
		assert(n != 0);
		return { x / n, y / n };
	}

	constexpr Vector2<T>& operator/= (const T& n)
	{
		assert(n != 0);

		x /= n; y /= n;
		return *this;
	}
};

//----- aliases -----
using Vector2f  = Vector2<float>;
using Vector2d  = Vector2<double>;
using Vector2i  = Vector2<int>;
using Vector2ui = Vector2<unsigned int>;