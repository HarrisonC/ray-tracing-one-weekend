#pragma once

#include <cmath>
#include <assert.h>
#include <ostream>
#include <algorithm>
#include <string>

template <typename T>
struct Vector3
{
	T x;
	T y;
	T z;

	constexpr Vector3()
	{
		x = 0; y = 0; z = 0;
	}

	constexpr Vector3(const T n)
	{
		x = n; y = n; z = n;
	}

	constexpr explicit Vector3(const T _x, const T _y, const T _z) :
		x(_x), y(_y), z(_z)
	{
	}

	//----- Utility Functions -----
	constexpr float MagnitudeSquared() const
	{
		return (x * x) + (y * y) + (z * z);
	}

	constexpr float Magnitude() const
	{
		return std::sqrt(MagnitudeSquared());
	}

	constexpr Vector3<T> Normalised() const
	{
		return *this / Magnitude();
	}

	constexpr T Dot(const Vector3<T>& rhs) const
	{
		return (x * rhs.x) + (y * rhs.y) + (z * rhs.z);
	}

	constexpr T AbsDot(const Vector3<T>& rhs) const
	{
		return std::abs(Dot(rhs));
	}

	constexpr Vector3<T> ProjectionOnto(const Vector3<T>& rhs) const
	{
		const float dot = Dot(rhs);
		return rhs * (dot / rhs.MagnitudeSquared());
	}

	constexpr Vector3<T> Cross(const Vector3<T>& rhs) const
	{
		return Vector3<T>
		{
			(y * rhs.z) - (z * rhs.y),
			(z * rhs.x) - (x * rhs.z),
			(x * rhs.y) - (y * rhs.x)
		};
	}

	constexpr void Swap(Vector3<T>& rhs)
	{
		Vector3<T> tmp = this;
		this = rhs;
		rhs = tmp;
	}

	constexpr bool IsZeroed() const
	{
		return x == 0 && y == 0 && z == 0;
	}

	//----- Accessor Functions -----
	constexpr T R() const
	{
		return x;
	}

	constexpr T G() const
	{
		return y;
	}

	constexpr T B() const
	{
		return z;
	}

	//----- Operator Overloads -----
	constexpr const T& operator[](int i) const
	{
		assert(i >= 0 && i < 3);

		if (i == 0) return x;
		if (i == 1) return y;
		return z;
	}

	constexpr T& operator[](int i)
	{
		assert(i >= 0 && i < 3);

		if (i == 0) return x;
		if (i == 1) return y;
		return z;
	}


	constexpr Vector3<T> operator-() const
	{
		return Vector3<T>{ -x, -y, -z };
	}

	constexpr bool operator== (const Vector3<T>& rhs) const
	{
		return x == rhs.x && y == rhs.y && z == rhs.z;
	}

	constexpr bool operator!= (const Vector3<T>& rhs) const
	{
		return x != rhs.x || y != rhs.y || z != rhs.z;
	}

	constexpr Vector3<T> operator+ (const Vector3<T>& rhs) const
	{
		return Vector3<T>{ x + rhs.x, y + rhs.y, z + rhs.z };
	}

	constexpr Vector3<T> operator+ (const T& n) const
	{
		return Vector3<T>{ x + n, y + n, z + n };
	}

	constexpr Vector3<T>& operator+= (const Vector3<T>& rhs)
	{
		x += rhs.x; y += rhs.y; z += rhs.z;
		return *this;
	}

	constexpr Vector3<T> operator- ( const Vector3<T>& rhs) const
	{
		return Vector3<T>{ x - rhs.x, y - rhs.y, z - rhs.z };
	}

	constexpr Vector3<T>& operator-= (const Vector3<T>& rhs)
	{
		x -= rhs.x; y -= rhs.y; z -= rhs.z;
		return *this;
	}

	constexpr Vector3<T> operator* (const Vector3<T>& rhs) const
	{
		return Vector3<T>{ x * rhs.x, y * rhs.y, z * rhs.z };
	}

	constexpr Vector3<T>& operator*= (const Vector3<T>& rhs)
	{
		x *= rhs.x; y *= rhs.y; z *= rhs.z;
		return *this;
	}

	constexpr Vector3<T>& operator*= (const T& n)
	{
		x *= n; y *= n; z *= n;
		return *this;
	}

	constexpr Vector3<T> operator/ (const Vector3<T>& rhs) const
	{
		assert(rhs.x != 0 && rhs.y != 0 && rhs.z != 0);
		return { x / rhs.x, y / rhs.y, z / rhs.z };
	}

	constexpr Vector3<T> operator/ (const T& n) const
	{
		assert(n != 0);
		return Vector3<T>{ x / n, y / n, z / n };
	}

	constexpr Vector3<T>& operator/= (const Vector3<T>& rhs)
	{
		assert(rhs.x != 0 && rhs.y != 0 && rhs.z != 0);

		x /= rhs.x; y /= rhs.y; z /= rhs.z;
		return *this;
	}

	constexpr Vector3<T>& operator/= (const T& n)
	{
		assert(n != 0);

		x /= n; y /= n; z /= n;
		return *this;
	}
};

template <typename T>
constexpr std::ostream& operator<< (std::ostream& os, const Vector3<T>& v)
{
	os << v.x << " " << v.y << " " << v.z;
	return os;
}

template <typename T>
constexpr Vector3<T> operator* (const T& n, const Vector3<T>& v)
{
	return Vector3<T>{ v.x * n, v.y * n, v.z * n };
}

template <typename T>
constexpr Vector3<T> operator* (const Vector3<T>& v, const T& n)
{
	return Vector3<T>{ v.x * n, v.y * n, v.z * n };
}

template <typename T>
constexpr Vector3<T> operator- (const T& n, const Vector3<T>& v)
{
	return Vector3<T>{ v.x - n, v.y - n, v.z - n };
}

template <typename T>
constexpr Vector3<T> operator- (const Vector3<T>& v, const T& n)
{
	return Vector3<T>{ v.x - n, v.y - n, v.z - n };
}

using Vector3f  = Vector3<float>;
using Vector3d  = Vector3<double>;
using Vector3i  = Vector3<int>;
using Vector3ui = Vector3<unsigned int>;